package ru.oklogic.bus55.map;

import android.content.Context;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

public class CustomMapController {
	private MapController mapController;
	
	public CustomMapController(MapController mapController) {
		
	}
	
	public MapController getMapController() {
		return mapController;
	}
	
	public OverlayManager getOverlayManager() {
		return mapController.getOverlayManager();
	}

	public Context getContext(){
		return getContext();
	}
	
	public ScreenPoint getScreenPoint(GeoPoint geoPoint){
		return mapController.getScreenPoint(geoPoint);
	}
	
	public GeoPoint getGeoPoint(ScreenPoint screenPoint){
		return mapController.getGeoPoint(screenPoint);
	}
}
