package ru.oklogic.bus55.activity.map.overlay.busroute.station;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;

import android.view.View;

import android.widget.LinearLayout;
import android.widget.TextView;

import ru.oklogic.bus55.R;

import ru.oklogic.bus55.activity.map.event.OnGeoStationClick;
import ru.oklogic.bus55.map.balloon.item.BaseBalloonItem;
import ru.oklogic.bus55.model.GeoStation;

public class StationBalloonItem extends BaseBalloonItem {
	GeoStation mGeoStation;

	OnGeoStationClick onRouteStartClick;
	OnGeoStationClick onRouteEndClick;
	OnGeoStationClick onPredictStationClick;

	public StationBalloonItem(GeoStation station, Context context) {
		super(context, station.getLatLong().toGeoPoint());

		this.mGeoStation = station;

		TextView nameView = new TextView(context);
		nameView.setTextAppearance(context,
				android.R.style.TextAppearance_Medium);
		nameView.setText(station.getName());
		nameView.setBackgroundResource(R.drawable.bg_map_balloon_clickable);
		
		TextView findRouteStart = new TextView(context);
		findRouteStart.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		findRouteStart.setText(context.getString(R.string.txt_findroute_start));
		findRouteStart.setBackgroundResource(R.drawable.bg_map_balloon_clickable);

		TextView findRouteEnd = new TextView(context);
		findRouteEnd.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		findRouteEnd.setText(context.getString(R.string.txt_findroute_end));
		findRouteEnd.setBackgroundResource(R.drawable.bg_map_balloon_clickable);
		
		try {
		    XmlResourceParser parser = getResources().getXml(R.drawable.text_color);
		    ColorStateList colors = ColorStateList.createFromXml(getResources(), parser);
		    findRouteEnd.setTextColor(colors);
		    findRouteStart.setTextColor(colors);
		    nameView.setTextColor(colors);
		} catch (Exception e) {}		

		nameView.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onPredictStationClick.onClick(mGeoStation);
			}
		});		
		findRouteStart.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onPredictStationClick.onClick(mGeoStation);
			}
		});		
		findRouteEnd.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onPredictStationClick.onClick(mGeoStation);
			}
		});
		
		LinearLayout internalLayout = new LinearLayout(context);
		internalLayout.setOrientation(LinearLayout.VERTICAL);
		internalLayout.addView(nameView);
//		internalLayout.addView(findRouteStart);
//		internalLayout.addView(findRouteEnd);
		
		this.addView(internalLayout);
		addCloseButton();
	}

	public void setOnPredictStationClick(OnGeoStationClick callback) {
		this.onPredictStationClick = callback;
	}
	
	public void setOnRouteStartClick(OnGeoStationClick callback) {
		this.onRouteStartClick = callback;
	}
	
	public void setOnRouteEndClick(OnGeoStationClick callback) {
		this.onRouteEndClick = callback;
	}
}
