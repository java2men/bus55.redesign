package ru.oklogic.bus55.activity.routes;

import java.util.LinkedList;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.HomeSherlockActivity;
import ru.oklogic.bus55.adapter.RouteGroupListAdapter;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.model.RouteGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class RouteGroupListActivity extends HomeSherlockActivity {
	RouteGroupListAdapter routeGroupListAdapter;

	protected String getActivityTitle() {
		return getResources().getString(R.string.route_title);
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_list_route_group);

		ListView list = (ListView) findViewById(R.id.route_group_list);
		routeGroupListAdapter = new RouteGroupListAdapter(this,
				new LinkedList<RouteGroup>(Bus55API.getRouteGroupList()));
		
		list.setAdapter(routeGroupListAdapter);

		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				goInsideGroup(view,
						routeGroupListAdapter.getByPosition(position));
			}
		});
	}

	public void goInsideGroup(View view, RouteGroup routeGroup) {
		Intent gotoDirectionViewActivity = new Intent(
				this.getApplicationContext(), RouteListActivity.class);
		gotoDirectionViewActivity.putExtra("group", routeGroup);
		this.startActivity(gotoDirectionViewActivity);
	}
}
