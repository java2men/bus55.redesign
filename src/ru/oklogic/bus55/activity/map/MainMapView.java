package ru.oklogic.bus55.activity.map;

import java.util.Collection;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.BundleTags;
import ru.oklogic.bus55.activity.HomeSherlockActivity;
import ru.oklogic.bus55.activity.map.controller.SuperController;
import ru.oklogic.bus55.activity.routes.RouteGroupListActivity;
import ru.oklogic.bus55.activity.stations.PredictActivity;
import ru.oklogic.bus55.activity.stations.StationActivity;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.Station;
import ru.oklogic.bus55.model.Util;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainMapView extends HomeSherlockActivity {

	MapController mMapController;
	
	SuperController superController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custommap);
		
		final MapView mapView = (MapView) findViewById(R.id.map);
		mapView.showBuiltInScreenButtons(true);

		mMapController = mapView.getMapController();
		mMapController.getOverlayManager().getMyLocation().setEnabled(true);
		if (savedInstanceState == null) {
			mMapController.setPositionNoAnimationTo(new GeoPoint(54.989636,
					73.364996));
		}
		
		superController = new SuperController(this);

		initLayers();
		readExtras();
	}

	@Override
	public void onPause() {
		super.onPause();
		superController.pause();
	}

	@Override
	public void onResume() {
		super.onResume();
		superController.resume();
	}

	protected void onDestroy() {
		super.onDestroy();
		superController.destroy();
	}

	private void initLayers() {
		for (BaseLayerController controller : superController.getControllers()) {
			Collection<BaseOverlay> overlays = controller.getOverlays();
			for (BaseOverlay overlay : overlays) {
				mMapController.getOverlayManager().addOverlay(overlay);
			}
		}
	}

	private void readExtras() {
		Bundle bundle = getIntent().getExtras();
		if (bundle == null)
			return;
		
		superController.readExtras(bundle);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		float zoom;
		boolean jams;
		GeoPoint geoPoint;
		GeoPoint geoPoint2;

		geoPoint = new GeoPoint(savedInstanceState.getDouble(BundleTags.LATITUDE),
				savedInstanceState.getDouble(BundleTags.LONGITUDE));

		geoPoint2 = new GeoPoint(
				savedInstanceState.getDouble(BundleTags.LATITUDE) + 0.0001,
				savedInstanceState.getDouble(BundleTags.LONGITUDE));

		zoom = savedInstanceState.getFloat(BundleTags.ZOOM);
		jams = savedInstanceState.getBoolean(BundleTags.JAMS);

		mMapController.setZoomCurrent(zoom);

		mMapController.setPositionNoAnimationTo(geoPoint2);
		mMapController.setPositionAnimationTo(geoPoint);
		mMapController.setJamsVisible(jams);
	}

	@Override
	public void onSaveInstanceState(Bundle state) {
		final MapView mapView = (MapView) findViewById(R.id.map);

		ScreenPoint screenPoint = new ScreenPoint(mapView.getWidth() / 2,
				mapView.getHeight() / 2);
		GeoPoint gp = mMapController.getGeoPoint(screenPoint);
		state.putDouble(BundleTags.LATITUDE, gp.getLat());
		state.putDouble(BundleTags.LONGITUDE, gp.getLon());
		state.putFloat(BundleTags.ZOOM, mMapController.getZoomCurrent());
		state.putBoolean(BundleTags.JAMS, mMapController.isJamsVisible());
	}

	public void gotoStationView(View view) {
		Intent gotoStationViewActivity = new Intent(
				this.getApplicationContext(), StationActivity.class);
		this.startActivity(gotoStationViewActivity);
	}

	public void gotoRouteGroupView(View view) {
		Intent gotoRouteGroupViewActivity = new Intent(
				this.getApplicationContext(), RouteGroupListActivity.class);
		this.startActivity(gotoRouteGroupViewActivity);
	}

	public void gotoPredictView(Station station, Station nextStation) {
		Intent gotoPredictViewActivity = new Intent(
				this.getApplicationContext(), PredictActivity.class);

		gotoPredictViewActivity.putExtra(BundleTags.DIRECTION,
				Util.getDirection(station, nextStation));
		this.startActivity(gotoPredictViewActivity);
	}
}