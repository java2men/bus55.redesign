package ru.oklogic.bus55.api;

public interface Callback<T> {
	void run(T data);
}
