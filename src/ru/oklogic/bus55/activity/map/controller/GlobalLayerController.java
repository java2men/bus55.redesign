package ru.oklogic.bus55.activity.map.controller;

import java.util.LinkedList;
import java.util.Collection;




import ru.oklogic.bus55.activity.map.BaseLayerController;
import ru.oklogic.bus55.activity.map.event.OnGeoStationClick;
import ru.oklogic.bus55.activity.map.event.OnLatLongClick;
import ru.oklogic.bus55.activity.map.event.OnMapClickListener;
import ru.oklogic.bus55.activity.map.overlay.global.ContextMenuBaloon;
import ru.oklogic.bus55.activity.map.overlay.global.KeyListenerOverlay;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.api.action.ParamNames;
import ru.oklogic.bus55.api.action.Parameters;
import ru.oklogic.bus55.map.balloon.BalloonController;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.GeoDirection;
import ru.oklogic.bus55.model.LatLong;

import ru.yandex.yandexmapkit.MapController;

public class GlobalLayerController extends BaseLayerController {
	KeyListenerOverlay keyListenerOverlay;
	LinkedList<BaseOverlay> overlays;

	Collection<OnMapClickListener> onLongPressListeners;
	OnLatLongClick onShowStationsHereClickListener;

	OnGeoStationClick onRouteStartClick;
	OnGeoStationClick onRouteEndClick;

	public GlobalLayerController(MapController mMapController,
			BalloonController balloonController) {
		super(mMapController, balloonController);
		keyListenerOverlay = new KeyListenerOverlay(mMapController);
		keyListenerOverlay.setOnLongPressListener(new OnMapClickListener() {
			public void onMapClick(LatLong latLong) {
				for (OnMapClickListener clickListerner : onLongPressListeners) {
					clickListerner.onMapClick(latLong);
				}
				showContextMenu(latLong);
			}
		});
		overlays = new LinkedList<BaseOverlay>();
		overlays.add(keyListenerOverlay);
		onLongPressListeners = new LinkedList<OnMapClickListener>();
	}

	public void setOnLongPressListener(OnMapClickListener onLongPressListener) {
		this.onLongPressListeners.add(onLongPressListener);
	}

	public void unsetOnLongPress(OnMapClickListener onLongPressListener) {
		this.onLongPressListeners.remove(onLongPressListener);
	}

	public void setOnShowStationsHereClick(OnLatLongClick onClickListener) {
		onShowStationsHereClickListener = onClickListener;
	}

	public void setOnRouteStartClick(OnGeoStationClick listener) {
		onRouteStartClick = listener;
	}

	public void setOnRouteEndClick(OnGeoStationClick listener) {
		onRouteEndClick = listener;
	}

	@Override
	public Collection<BaseOverlay> getOverlays() {
		return overlays;
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	private void showContextMenu(LatLong latLong) {
		ContextMenuBaloon contextMenuBalloon = new ContextMenuBaloon(latLong,
				getMapController().getContext());
		contextMenuBalloon
				.setOnShowStationsHereClickListener(new OnLatLongClick() {
					public void onClick(LatLong latLong) {
						showStationHereClick(latLong);
					}
				});
		contextMenuBalloon.setOnRouteStartClickListener(new OnLatLongClick() {
			public void onClick(LatLong latLong) {
				onRouteStartClickHandler(latLong);
			}
		});
		contextMenuBalloon.setOnRouteEndClickListener(new OnLatLongClick() {
			public void onClick(LatLong latLong) {
				onRouteEndClickHandler(latLong);
			}
		});
		getBalloonController().show(contextMenuBalloon);
	}

	private void showStationHereClick(LatLong latLong) {
		onShowStationsHereClickListener.onClick(latLong);
	}

	private void onRouteStartClickHandler(final LatLong latLong) {
		Parameters params = new Parameters();
		params.put(ParamNames.LOCATION, latLong);
		Bus55API.getDirectionsGeoLoc(params,
				new Callback<Collection<GeoDirection>>() {
					public void run(Collection<GeoDirection> data) {
						GeoDirection bestDirection = null;
						Double bestDistance = Double.MAX_VALUE;
						for (GeoDirection direction : data) {
							LatLong directionLatLong = direction.getStation()
									.getLatLong();
							Double distance = latLong
									.getPseudoDistance(directionLatLong);
							if (distance < bestDistance) {
								bestDistance = distance;
								bestDirection = direction;
							}
						}
						onRouteStartClick.onClick(bestDirection.getStation());
					}

				}, getMapController().getContext());
	}

	private void onRouteEndClickHandler(final LatLong latLong) {
		Parameters params = new Parameters();
		params.put(ParamNames.LOCATION, latLong);
		Bus55API.getDirectionsGeoLoc(params,
				new Callback<Collection<GeoDirection>>() {
					public void run(Collection<GeoDirection> data) {
						GeoDirection bestDirection = null;
						Double bestDistance = Double.MAX_VALUE;
						for (GeoDirection direction : data) {
							LatLong directionLatLong = direction.getStation()
									.getLatLong();
							Double distance = latLong
									.getPseudoDistance(directionLatLong);
							if (distance < bestDistance) {
								bestDistance = distance;
								bestDirection = direction;
							}
						}
						onRouteStartClick.onClick(bestDirection.getStation());
					}

				}, getMapController().getContext());
	}
}
