package ru.oklogic.bus55.activity.map.overlay.busroute;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import ru.oklogic.bus55.activity.map.event.OnGeoVehicleClick;
import ru.oklogic.bus55.activity.map.overlay.busroute.vehicle.VehicleOverlayItem;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.map.overlay.item.CustomRenderer;
import ru.oklogic.bus55.model.GeoVehicle;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import android.content.res.Resources;

public class BusRouteVehicleOverlay extends BaseOverlay {
	HashMap<Integer, VehicleOverlayItem> vehicleOverlayItems;

	OnGeoVehicleClick onVehicleClick;

	public BusRouteVehicleOverlay(MapController mMapController) {
		super(mMapController);
		setIRender(new CustomRenderer());

		vehicleOverlayItems = new HashMap<Integer, VehicleOverlayItem>();
	}

	public void setOnVehicleClick(OnGeoVehicleClick onVehicleClick) {
		this.onVehicleClick = onVehicleClick;
	}

	public void onItemClick(OverlayItem overlayItem) {
		if (onVehicleClick == null)
			return;
		VehicleOverlayItem mVehicleOverlayItem = (VehicleOverlayItem) overlayItem;
		onVehicleClick.onClick(mVehicleOverlayItem.getGeoVehicle());
	}

	public void updateVehicles(Collection<GeoVehicle> vehicles) {
		synchronized (vehicleOverlayItems) {
			for (GeoVehicle vehicle : vehicles) {
				if (vehicleOverlayItems.containsKey(vehicle.getId())) {
					VehicleOverlayItem overlayItem = vehicleOverlayItems
							.get(vehicle.getId());
					overlayItem.getGeoVehicle().setLatLong(vehicle.getLatLong());
					overlayItem.setGeoPoint(vehicle.getLatLong().toGeoPoint());
					continue;
				}

				Resources res = getResources();

				VehicleOverlayItem busVehicleOverlayItem = new VehicleOverlayItem(
						vehicle, VehicleOverlayItem.getBitmap(vehicle, res));

				vehicleOverlayItems.put(vehicle.getId(), busVehicleOverlayItem);
				addOverlayItem(busVehicleOverlayItem);
			}

			Collection<VehicleOverlayItem> vOverlayItems = vehicleOverlayItems
					.values();
			
			Collection<Integer> vOverlayKeysToRemove = new LinkedList<Integer>();

			for (VehicleOverlayItem overlayItem : vOverlayItems) {
				Boolean contains = false;
				for (GeoVehicle geoVehicle : vehicles) {
					GeoVehicle geoVehicle2 = overlayItem.getGeoVehicle();
					if (geoVehicle2.getId().equals(geoVehicle.getId())) {
						contains = true;
						break;
					}
				}
				if (!contains) {
					vOverlayKeysToRemove.add(overlayItem.getGeoVehicle()
							.getId());
					removeOverlayItem(overlayItem);
				}
			}
			
			for (Integer key: vOverlayKeysToRemove) {
				vehicleOverlayItems.remove(key);
			}

		}
	}
}
