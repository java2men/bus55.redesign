package ru.oklogic.bus55.activity.stations;

import java.util.Collection;
import java.util.LinkedList;

import ru.oklogic.bus55.BundleTags;
import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.SecondarySherlockActivity;
import ru.oklogic.bus55.activity.routes.RouteActivity;
import ru.oklogic.bus55.adapter.PredictListAdapter;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.api.action.ParamNames;
import ru.oklogic.bus55.api.action.Parameters;
import ru.oklogic.bus55.db.service.FavoriteDirectionsService;
import ru.oklogic.bus55.model.Direction;
import ru.oklogic.bus55.model.Predict;
import ru.oklogic.bus55.model.Route;
import ru.oklogic.bus55.util.UtilStringBuilder;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;

public class PredictActivity extends SecondarySherlockActivity {
	final Integer ACTION_STAR = 1;
	final Integer ACTION_REFRESH = 2;

	Direction direction;
	PredictListAdapter predictListAdapter;
	AQuery updatePredictAjax;
	ListView predictListView;

	FavoriteDirectionsService favoritesService;

	protected String getActivityTitle() {
		return UtilStringBuilder.GetPredictName(direction, getResources());
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		direction = (Direction) getIntent().getExtras().getSerializable(
				BundleTags.DIRECTION);

		super.onCreate(savedInstanceState);

		setContentView(R.layout.act_station_predict);
		favoritesService = new FavoriteDirectionsService(this);

		predictListView = (ListView) findViewById(R.id.predict_list);
		predictListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Predict predict = predictListAdapter.getByPosition(position);
				gotoRouteView(view, predict.getRoute());
			}
		});
		predictListAdapter = new PredictListAdapter(this,
				new LinkedList<Predict>());
		predictListView.setAdapter(predictListAdapter);

		updatePredict();
	}

	public void onResume() {
		super.onResume();
	}

	public void onRefreshClick(View view) {
		updatePredict();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(Menu.NONE, ACTION_STAR, Menu.NONE, R.string.btn_favorite)
				.setIcon(
						favoritesService.isDirectionFavorite(direction) ? R.drawable.btn_actionbar_star_checked
								: R.drawable.btn_actionbar_star)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add(Menu.NONE, ACTION_REFRESH, Menu.NONE, R.string.btn_refresh)
				.setIcon(R.drawable.ic_refresh)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == ACTION_REFRESH) {
			updatePredict();
		} else if (id == ACTION_STAR) {
			toggleFavorites();
		} else {
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void updatePredict() {
		Parameters params = new Parameters();
		params.put(ParamNames.DIRECTION, direction);
		showLoading();
		invalidateOptionsMenu();

		updatePredictAjax = Bus55API.getPredict(params,
				new Callback<Collection<Predict>>() {
					public void run(Collection<Predict> data) {
						hideLoading();
						invalidateOptionsMenu();
						predictListAdapter.clear();
						loadData(data);
					}
				}, this);
	}

	private void loadData(Collection<Predict> data) {
		predictListAdapter = new PredictListAdapter(this,
				new LinkedList<Predict>(data));
		predictListView.setAdapter(predictListAdapter);
	}

	public void onCancelLoading() {
		updatePredictAjax.clear();
	}

	public void gotoRouteView(View view, Route route) {
		Intent gotoRouteViewActivity = new Intent(this.getApplicationContext(),
				RouteActivity.class);
		gotoRouteViewActivity.putExtra("route", route);
		this.startActivity(gotoRouteViewActivity);
	}

	public void toggleFavorites() {
		if (!favoritesService.isDirectionFavorite(direction)) {
			favoritesService.addDirectionToFavorite(direction);
			invalidateOptionsMenu();
		} else {
			favoritesService.removeDirectionFromFavorites(direction);
			invalidateOptionsMenu();
		}
	}
}
