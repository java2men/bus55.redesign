package ru.oklogic.bus55.activity.navigation;

import android.content.Context;
import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.favorites.FavoriteDirectionsActivity;

public class NavigationFavoritesItem extends NavigationItem {
	Context context;	
	public NavigationFavoritesItem(Context context) {
		this.context = context;
	}
	
	public int getIcon() {
		return R.drawable.btn_main_star;
	}
	public Class<?> getActivityClass() {
		return FavoriteDirectionsActivity.class;
	}

	@Override
	public String getTitle() {
		return context.getString(R.string.favorites_button);
	}
}
