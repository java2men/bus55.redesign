package ru.oklogic.bus55.activity.navigation;

import android.content.Context;
import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.stations.StationActivity;

public class NavigationStationsItem extends NavigationItem {
	Context context;

	public NavigationStationsItem (Context context) {
		this.context = context;
	}

	public int getIcon() {
		return R.drawable.btn_main_busstation;
	}

	public Class<?> getActivityClass() {
		return StationActivity.class;
	}

	@Override
	public String getTitle() {
		return context.getString(R.string.stations_button);
	}
}
