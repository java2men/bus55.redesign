package ru.oklogic.bus55.adapter;

import java.util.List;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.model.RouteGroup;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class RouteGroupListAdapter extends ArrayAdapter<RouteGroup> {
	final List<RouteGroup> items;
	final Activity context;
	
	Filter filter;
	
    public RouteGroup getByPosition(int position){
        return items.get(position);
    }	

	public RouteGroupListAdapter(Activity context, List<RouteGroup> items) {
		super(context, R.layout.listitem_routegroup, items);
		this.context = context;
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflator = context.getLayoutInflater();
		View view = inflator.inflate(R.layout.listitem_routegroup, parent, false);
		TextView routeGroupName = (TextView) view.findViewById(R.id.route_group_list_item_name);

		routeGroupName.setText(getByPosition(position).getName()+"x");
		return view;
	}
}
