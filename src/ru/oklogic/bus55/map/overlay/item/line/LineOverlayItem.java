package ru.oklogic.bus55.map.overlay.item.line;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import ru.oklogic.bus55.map.overlay.item.BaseOverlayItem;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

public class LineOverlayItem extends BaseOverlayItem {
	GeoPoint geoPoint1;
	GeoPoint geoPoint2;
	int color;
	float width;
	MapController mapController;

	public LineOverlayItem(
			GeoPoint geoPoint1, 
			GeoPoint geoPoint2,
			Bitmap bitmap, 
			int color, 
			float width,
			MapController mapController) {

		super(new GeoPoint(
				(geoPoint1.getLat() + geoPoint2.getLat()) / 2.0f,
				(geoPoint1.getLon() + geoPoint2.getLon()) / 2.0f
				), bitmap);
		
		this.mapController = mapController;
		this.geoPoint1 = geoPoint1;
		this.geoPoint2 = geoPoint2;
		this.color = color;
		this.width = width; 
	}

	public GeoPoint getGeoPoint1() {
		return geoPoint1;
	}

	public void setGeoPoint1(GeoPoint geoPoint1) {
		this.geoPoint1 = geoPoint1;
	}

	public GeoPoint getGeoPoint2() {
		return geoPoint2;
	}

	public void setGeoPoint2(GeoPoint geoPoint2) {
		this.geoPoint2 = geoPoint2;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public void draw(Canvas canvas) {
		ScreenPoint p1 = mapController.getScreenPoint(getGeoPoint1());
		ScreenPoint p2 = mapController.getScreenPoint(getGeoPoint2());
		
		Paint paint = new Paint();
		paint.setColor(getColor());
		paint.setStrokeWidth(getWidth());
		
		canvas.drawLine(p1.getX(), p1.getY(), p2.getX(), p2.getY(), paint);
	}
}
