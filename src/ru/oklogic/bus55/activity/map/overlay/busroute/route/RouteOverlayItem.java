package ru.oklogic.bus55.activity.map.overlay.busroute.route;

import java.util.Iterator;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.map.overlay.item.BaseOverlayItem;
import ru.oklogic.bus55.model.LatLong;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

public class RouteOverlayItem extends BaseOverlayItem {
	List<LatLong> routePath;
	MapController mapController;

	public byte getPriority() {
		return 1;
	}

	public RouteOverlayItem(GeoPoint geoPoint, Bitmap bitmap,
			List<LatLong> routePath, MapController mapController) {
		super(geoPoint, bitmap);
		this.mapController = mapController;
		this.routePath = routePath;
	}

	public void draw(Canvas canvas) {
		Path canvasPath = new Path();

		Iterator<LatLong> iterator = routePath.iterator();
		LatLong firstPoint = iterator.next();
		ScreenPoint firstScreenPoint = mapController.getScreenPoint(firstPoint
				.toGeoPoint());
		canvasPath.moveTo(firstScreenPoint.getX(), firstScreenPoint.getY());
		
		while(iterator.hasNext())
		{
			LatLong point = iterator.next();
			ScreenPoint screenPoint = mapController.getScreenPoint(point
					.toGeoPoint());
			canvasPath.lineTo(screenPoint.getX(), screenPoint.getY());
		}

		Paint paint = new Paint();
		paint.setColor(mapController.getContext().getResources()
				.getColor(R.color.base));

		paint.setStrokeWidth(3f);
		
		paint.setStyle(Style.STROKE);
		
		canvas.drawPath(canvasPath, paint);
	}
}
