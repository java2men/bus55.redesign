package ru.oklogic.bus55.activity.map;

import java.util.Collection;

import android.content.res.Resources;
import android.os.Bundle;

import ru.oklogic.bus55.map.balloon.BalloonController;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.LatLong;
import ru.yandex.yandexmapkit.MapController;

public abstract class BaseLayerController {
	MapController mMapController;
	BalloonController balloonController;

	public BaseLayerController(MapController mMapController,
			BalloonController balloonController) {
		this.mMapController = mMapController;
		this.balloonController = balloonController;
	}

	public void readExtras(Bundle bundle) {

	}

	public void setMyLocation(LatLong latLong) {
	}

	abstract public Collection<BaseOverlay> getOverlays();

	abstract public void pause();

	abstract public void resume();

	protected MapController getMapController() {
		return mMapController;
	}

	protected BalloonController getBalloonController() {
		return balloonController;
	}

	protected void setMapController(MapController mMapController) {
		this.mMapController = mMapController;
	}

	protected Resources getResources() {
		return mMapController.getContext().getResources();
	}

}
