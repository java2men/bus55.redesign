package ru.oklogic.bus55.activity;

import java.util.LinkedList;
import java.util.List;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.menu.RibbonMenuController;
import ru.oklogic.bus55.activity.navigation.NavigationFavoritesItem;
import ru.oklogic.bus55.activity.navigation.NavigationItem;
import ru.oklogic.bus55.activity.navigation.NavigationMapItem;
import ru.oklogic.bus55.activity.navigation.NavigationRoutesItem;
import ru.oklogic.bus55.activity.navigation.NavigationStationsItem;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.darvds.ribbonmenu.iRibbonMenuVisibilityChangeCallback;

public class HomeSherlockActivity extends SherlockActivity implements
		iRibbonMenuVisibilityChangeCallback {

	protected final String ACTIVITY_PREFERENCES = "activityPreferences";
	protected final String LAST_ACTIVITY = "lastActivity";
	protected Integer currentActivityPosition = null;

	// private RibbonMenuView rbmView;
	private RibbonMenuController ribbonMenuController;

	protected String getActivityTitle() {
		return getResources().getString(R.string.activity_name);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Sherlock_Light_DarkActionBar);
		super.onCreate(savedInstanceState);
		getSupportActionBar().setBackgroundDrawable(
				getResources().getDrawable(R.drawable.bg_tbar));
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setIcon(R.drawable.ic_bus);

		initNavigation();
	}

	@Override
	public void onResume() {
		super.onResume();
		getSupportActionBar()
				.setSelectedNavigationItem(currentActivityPosition);

	}

	public void onPause() {
		hideLoading();
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	protected void toggleRibbonMenu() {
		ribbonMenuController.toggle();
	}

	protected void showRibbonMenu() {
		ribbonMenuController.show();
	}

	protected void hideRibbonMenu() {
		ribbonMenuController.hide();
	}

	protected void initRibbonMenu() {
		if (ribbonMenuController != null)
			return;
		ribbonMenuController = new RibbonMenuController();
		ribbonMenuController.init(this);
		ribbonMenuController.setHideMenuCallback(this);
		ribbonMenuController.setShowMenuCallback(this);
	}

	protected void showLoading() {
		setSupportProgress(Window.PROGRESS_END);
		setSupportProgressBarIndeterminateVisibility(true);
	}

	protected void hideLoading() {
		setSupportProgressBarIndeterminateVisibility(false);
	}

	private void initNavigation() {
		List<NavigationItem> navigation_list = new LinkedList<NavigationItem>();
		navigation_list.add(new NavigationStationsItem(this));
		navigation_list.add(new NavigationRoutesItem(this));
		navigation_list.add(new NavigationMapItem(this));
		navigation_list.add(new NavigationFavoritesItem(this));

		final ArrayAdapter<NavigationItem> adapter = new ArrayAdapter<NavigationItem>(
				this, R.layout.listitem_navigation, R.id.title,
				navigation_list);

		currentActivityPosition = 0;
		for (NavigationItem item : navigation_list) {
			if (this.getClass().equals(item.getActivityClass())) {
				break;
			}
			++currentActivityPosition;
		}

		getSupportActionBar().setListNavigationCallbacks(adapter,
				new OnNavigationListener() {
					public boolean onNavigationItemSelected(int position,
							long itemId) {
						Class<?> cls = adapter.getItem(position)
								.getActivityClass();
						if (HomeSherlockActivity.this.getClass().equals(cls))
							return true;
						Intent intent = new Intent(HomeSherlockActivity.this,
								cls);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						HomeSherlockActivity.this.startActivity(intent);
						return false;
					}
				});
		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		getSupportActionBar()
				.setSelectedNavigationItem(currentActivityPosition);
	}

	@Override
	public void onVisibilityChange(Boolean shown) {
		// this.getSupportActionBar().setDisplayHomeAsUpEnabled(!shown);
	}
}
