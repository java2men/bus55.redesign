package ru.oklogic.bus55.map.overlay.item;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

public class BaseOverlayItem extends OverlayItem {

	public BaseOverlayItem(GeoPoint geoPoint, Bitmap bitmap) {
		super(geoPoint, bitmap);
	}
	
	protected boolean isClickable() {
		return false;
	}
	
	protected float getClickAreaSize() {
		return 30f;
	}
	
	protected float getClickAreaWidth() {
		return getClickAreaSize();
	}
	
	protected float getClickAreaHeight() {
		return getClickAreaSize();
	}
	
	public boolean isClicked(float x, float y) {
		if (!isClickable())
			return false;
		
		ScreenPoint screenPoint = getScreenPoint();
		
		if (Math.abs(x - screenPoint.getX()) > getClickAreaWidth()) {
			return false;
		}
		
		if (Math.abs(y - screenPoint.getY()) > getClickAreaHeight()) {
			return false;
		}
		
		return true;
	}

	public void draw(Canvas canvas) {
		int i = getOffsetCenterX() + getOffsetX();
		int j = getOffsetCenterY() + getOffsetY();

		canvas.drawBitmap(getBitmap(), (int) (getScreenPoint().getX() + i),
				(int) (getScreenPoint().getY() + j), new Paint());

	}

	public int compareTo(OverlayItem o) {
		return a(o);
	}

	public int a(OverlayItem paramOverlayItem) {
		byte b1 = getPriority();
		byte b2 = paramOverlayItem.getPriority();

		if (b1 < b2)
			return -1;
		if (b1 > b2)
			return 1;

		long l1 = (int) getPoint().y;
		long l2 = (int) paramOverlayItem.getPoint().y;

		if (l1 < l2)
			return -1;
		if (l1 > l2)
			return 1;

		return 0;
	}
}
