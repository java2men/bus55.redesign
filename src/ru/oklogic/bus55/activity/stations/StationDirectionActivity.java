package ru.oklogic.bus55.activity.stations;

import java.util.LinkedList;
import java.util.Collection;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.SecondarySherlockActivity;
import ru.oklogic.bus55.adapter.StationsDirectionListAdapter;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.api.action.ParamNames;
import ru.oklogic.bus55.api.action.Parameters;
import ru.oklogic.bus55.model.Direction;
import ru.oklogic.bus55.model.Station;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.androidquery.AQuery;

public class StationDirectionActivity extends SecondarySherlockActivity {
	Station station;
	ListView listView;
	StationsDirectionListAdapter directionListAdapter;
	AQuery getDirectionsAjax;

	protected String getActivityTitle() {
		return station.getName();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		station = (Station) getIntent().getExtras().getSerializable("station");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_station_direction);

		listView = (ListView) findViewById(R.id.stations_direction_list);
		directionListAdapter = new StationsDirectionListAdapter(this,
				new LinkedList<Direction>());
		listView.setAdapter(directionListAdapter);

		updateDirections();
		initEventListeners();
	}

	private void gotoPredict(View view, Direction direction) {
		Intent gotoPredictViewActivity = new Intent(
				this.getApplicationContext(), PredictActivity.class);

		gotoPredictViewActivity.putExtra("direction", direction);
		this.startActivity(gotoPredictViewActivity);
	}

	private void initEventListeners() {
		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				gotoPredict(view, directionListAdapter.getByPosition(position));
			}
		});
	}

	private void updateDirections() {
		Parameters params = new Parameters();
		params.put(ParamNames.STATION, station);
		showLoading();

		getDirectionsAjax = Bus55API.getDirectionList(params,
				new Callback<Collection<Direction>>() {
					public void run(Collection<Direction> data) {
						loadData(data);
						hideLoading();
					}
				}, this);
	}

	private void loadData(Collection<Direction> data) {
		directionListAdapter = new StationsDirectionListAdapter(this, new LinkedList<Direction>(data));
		listView.setAdapter(directionListAdapter);
	}

	public void onCancelLoading() {
		getDirectionsAjax.clear();
	}
}
