package ru.oklogic.bus55.db.service;

import java.util.Collection;
import java.util.LinkedList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import ru.oklogic.bus55.db.DatabaseTags;
import ru.oklogic.bus55.db.fields.FavoriteDirectionsFields;
import ru.oklogic.bus55.db.helper.DatabaseHelper;
import ru.oklogic.bus55.model.Direction;
import ru.oklogic.bus55.model.Station;

public class FavoriteDirectionsService {
	DatabaseHelper databaseHelper;

	public FavoriteDirectionsService(Context context) {
		databaseHelper = new DatabaseHelper(context);
		databaseHelper.getWritableDatabase();
	}

	public Collection<Direction> getFavoriteDirections() {
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		try {
			Cursor cursor = db.query(
					DatabaseTags.FAVORITE_DIRECTIONS_TABLE_NAME, null, null,
					null, null, null, FavoriteDirectionsFields.STATION_NAME);

			try {
				Collection<Direction> directions = new LinkedList<Direction>();

				while (cursor.moveToNext()) {
					String stationId = cursor
							.getString(cursor
									.getColumnIndex(FavoriteDirectionsFields.STATION_ID));
					String stationName = cursor
							.getString(cursor
									.getColumnIndex(FavoriteDirectionsFields.STATION_NAME));
					String nextStationId = cursor
							.getString(cursor
									.getColumnIndex(FavoriteDirectionsFields.NEXT_STATION_ID));
					String nextStationName = cursor
							.getString(cursor
									.getColumnIndex(FavoriteDirectionsFields.NEXT_STATION_NAME));
					String description = cursor
							.getString(cursor
									.getColumnIndex(FavoriteDirectionsFields.DESCRIPTION));
					String directionId = cursor
							.getString(cursor
									.getColumnIndex(FavoriteDirectionsFields.DIRECTION_ID));

					Station station = new Station();
					station.setId(stationId);
					station.setName(stationName);

					Station nextStation = new Station();
					nextStation.setId(nextStationId);
					nextStation.setName(nextStationName);

					Direction direction = new Direction();
					direction.setDescription(description);
					direction.setId(directionId);
					direction.setStation(station);
					direction.setNextStation(nextStation);

					directions.add(direction);
				}

				return directions;
			} finally {
				cursor.close();
			}
		} finally {
			db.close();
		}
	}

	public boolean isDirectionFavorite(Direction direction) {
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		try {
			String where = FavoriteDirectionsFields.STATION_ID + " =? AND "
					+ FavoriteDirectionsFields.NEXT_STATION_ID + " =? ";
			Station nextStation = direction.getNextStation();
			if (nextStation == null) {
				nextStation = direction.getStation();
			}
			Cursor cursor = db.query(
					DatabaseTags.FAVORITE_DIRECTIONS_TABLE_NAME,
					new String[] { FavoriteDirectionsFields.DIRECTION_ID },
					where, new String[] { direction.getStation().getId(),
							nextStation.getId() }, null, null, null, null);
			try {
				Boolean isFavorite = cursor.getCount() > 0;
				return isFavorite;
			} finally {
				cursor.close();
			}
		} finally {
			db.close();
		}
	}

	public void addDirectionToFavorite(Direction direction) {
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();

			values.put(FavoriteDirectionsFields.STATION_ID, direction
					.getStation().getId());
			values.put(FavoriteDirectionsFields.STATION_NAME, direction
					.getStation().getName());
			values.put(FavoriteDirectionsFields.NEXT_STATION_ID, direction
					.getNextStation().getId());
			values.put(FavoriteDirectionsFields.NEXT_STATION_NAME, direction
					.getNextStation().getName());
			values.put(FavoriteDirectionsFields.DESCRIPTION,
					direction.getDirectionDescription());
			values.put(FavoriteDirectionsFields.DIRECTION_ID, direction.getId());
			db.insert(DatabaseTags.FAVORITE_DIRECTIONS_TABLE_NAME, null, values);
		} finally {
			db.close();
		}
	}

	public void removeDirectionFromFavorites(Direction direction) {
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		try {
			Station nextStation = direction.getNextStation();
			if (nextStation == null) {
				nextStation = direction.getStation();
			}
			String where = FavoriteDirectionsFields.STATION_ID + " =? AND "
					+ FavoriteDirectionsFields.NEXT_STATION_ID + " =? ";
			db.delete(
					DatabaseTags.FAVORITE_DIRECTIONS_TABLE_NAME,
					where,
					new String[] { direction.getStation().getId(),
							nextStation.getId() });
		} finally {
			db.close();
		}
	}
}
