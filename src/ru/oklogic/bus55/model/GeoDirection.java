package ru.oklogic.bus55.model;

import java.io.Serializable;

public class GeoDirection extends Direction implements Serializable {
	private static final long serialVersionUID = 353765446069129270L;
	
	GeoStation station;
	GeoStation nextStation;
	
	String directionDescription;
	String directionId;
	
	public GeoDirection(){}
	
	public GeoDirection(
			String directionId,
			GeoStation station,
			GeoStation nextStation,
			String directionDescription) {
		this.directionId = directionId;
		this.station = station;
		this.nextStation = nextStation;
		this.directionDescription = directionDescription;
	}
	
	public String getId(){
		return this.directionId;
	}
	
	public void setId(String directionId){
		this.directionId = directionId;
	}
	
	public GeoStation getNextStation(){
		return this.nextStation;
	}
	
	public void setNextStation(Station nextStation){
		this.nextStation = (GeoStation)nextStation;
	}
	
	public GeoStation getStation(){
		return this.station;
	}
	
	public void setStation(Station station){
		this.station = (GeoStation)station;
	}
	
	public String getDirectionDescription(){
		return directionDescription;
	}
	
	public void setDescription(String directionDescription){
		this.directionDescription = directionDescription;
	}
}
