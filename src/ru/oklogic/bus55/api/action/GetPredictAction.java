package ru.oklogic.bus55.api.action;

import java.util.LinkedList;
import java.util.Collection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ru.oklogic.bus55.model.Direction;
import ru.oklogic.bus55.model.Predict;
import ru.oklogic.bus55.model.Route;
import ru.oklogic.bus55.model.Route.Type;
import ru.oklogic.bus55.util.UtilStringBuilder;

public class GetPredictAction extends BaseAction<Collection<Predict>> {
	@Override
	protected String getUrl(Parameters params) {
		Direction direction = (Direction) params.get(ParamNames.DIRECTION);

		return "http://bus.admomsk.ru/index.php/prediction/station/"
				+ direction.getId() + "/null";
	}

	@Override
	protected Collection<Predict> parse(String response, Parameters params) {
		Collection<Predict> predict = new LinkedList<Predict>();
		try {
			Document dom = parseXML("<root>" + response + "</root>");
			Element root = dom.getDocumentElement();
			NodeList predictItems = root.getElementsByTagName("div");

			for (Integer index = 0; index < predictItems.getLength(); ++index) {
				Element stationElement = (Element) predictItems.item(index);
				if (!stationElement.getAttribute("class").contains("marshrut")) {
					continue;
				}
				NodeList subdiv = stationElement.getElementsByTagName("div");
				int start = 0;
				if (((Element)subdiv.item(0)).getAttribute("class").contains(("marshrut")) ) start = 1;
				Element routeInfoElement = (Element) subdiv.item(start);
				String routeClass = routeInfoElement.getAttribute("class");
				Element routeNameElement = (Element) routeInfoElement
						.getElementsByTagName("a").item(0);
				Element descriptionElement = (Element) subdiv.item(start+1);
				Element descriptionElement1 = (Element) descriptionElement
						.getElementsByTagName("a").item(0);
				Element descriptionElement2 = (Element) descriptionElement
						.getElementsByTagName("a").item(1);
				Element predictionElement = (Element) subdiv.item(start+2);

				Route route = new Route();
				route.setName(UtilStringBuilder
						.GetContentText(routeNameElement));
				route.setId(routeNameElement.getAttribute("id"));
				route.setDescription(UtilStringBuilder
						.GetContentText(descriptionElement1)
						+ " - "
						+ UtilStringBuilder.GetContentText(descriptionElement2));

				if (routeClass.contains("troll")) {
					route.setType(Type.TROLL);
				} else if (routeClass.contains("tram")) {
					route.setType(Type.TRAM);
				} else {
					route.setType(Type.BUS);
				}

				Predict predictItem = new Predict();
				predictItem.setRoute(route);
				predictItem.setDate(UtilStringBuilder
						.GetContentText(predictionElement));
				predict.add(predictItem);
			}
		} catch (Exception e) {

		}
		return predict;
	}
}
