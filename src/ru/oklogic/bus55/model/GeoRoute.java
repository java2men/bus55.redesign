package ru.oklogic.bus55.model;

import java.util.LinkedList;
import java.util.List;

public class GeoRoute extends Route{
	private static final long serialVersionUID = -3431928207943313181L;
	
	List<LatLong> path;
	List<GeoStation> stations;
	public GeoRoute(){
		path = new LinkedList<LatLong>();
		stations = new LinkedList<GeoStation>();
	}
	public List<LatLong> getPath() {
		return path;
	}
	public void setPath(List<LatLong> path) {
		this.path = path;
	}
	public List<GeoStation> getStations() {
		return stations;
	}
	public void setStations(List<GeoStation> stations) {
		this.stations = stations;
	}
}
