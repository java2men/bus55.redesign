package ru.oklogic.bus55.activity;

import ru.oklogic.bus55.R;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;

public class SecondarySherlockActivity extends SherlockActivity {
	
	private Boolean loading =  false;

	protected String getActivityTitle() {
		return getResources().getString(R.string.activity_name);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Sherlock_Light_DarkActionBar);
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		
		this.getSupportActionBar().setTitle(getActivityTitle());
		this.getSupportActionBar().setBackgroundDrawable(
				getResources().getDrawable(R.drawable.bg_tbar));
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		this.getSupportActionBar().setIcon(R.drawable.ic_bus);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	public void onPause() {
		hideLoading();
		super.onPause();
	}
	
	protected Boolean isLoading() {
		return loading;
	}

	protected void showLoading() {
		loading = true;
		setSupportProgressBarIndeterminateVisibility(true);
	}

	protected void hideLoading() {
		loading = false;
		setSupportProgressBarIndeterminateVisibility(false);
	}
}
