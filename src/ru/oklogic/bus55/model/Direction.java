package ru.oklogic.bus55.model;

import java.io.Serializable;

public class Direction implements Serializable {
	private static final long serialVersionUID = 353765446069129270L;
	
	Station station;
	Station nextStation;
	
	String directionDescription;
	String directionId;
	
	public Direction(){}
	
	public Direction(
			String directionId,
			Station station,
			Station nextStation,
			String directionDescription) {
		this.directionId = directionId;
		this.station = station;
		this.nextStation = nextStation;
		this.directionDescription = directionDescription;
	}
	
	public String getId(){
		return this.directionId;
	}
	
	public void setId(String directionId){
		this.directionId = directionId;
	}
	
	public Station getNextStation(){
		return this.nextStation;
	}
	
	public void setNextStation(Station nextStation){
		this.nextStation = nextStation;
	}
	
	public Station getStation(){
		return this.station;
	}
	
	public void setStation(Station station){
		this.station = station;
	}
	
	public String getDirectionDescription(){
		return directionDescription;
	}
	
	public void setDescription(String directionDescription){
		this.directionDescription = directionDescription;
	}
}
