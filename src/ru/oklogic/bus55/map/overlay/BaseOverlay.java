package ru.oklogic.bus55.map.overlay;

import java.util.Iterator;

import android.content.res.Resources;
import ru.oklogic.bus55.map.overlay.item.BaseOverlayItem;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;

public class BaseOverlay extends Overlay {
	public BaseOverlay(MapController controller) {
		super(controller);
	}

	public int compareTo(Object arg0) {
		if (!(arg0 instanceof BaseOverlay))
			return -1;
		return 0;
	}

	protected Resources getResources() {
		return getMapController().getContext().getResources();
	}

	protected OverlayItem a(float x, float y) {
		Iterator<?> localIterator = getPrepareDrawList().iterator();
		
		while (localIterator.hasNext()) {
			
			OverlayItem localOverlayItem = (OverlayItem) localIterator.next();
			
			Boolean clicked = isClicked(localOverlayItem, x, y); 
			
			if (clicked) {
				return localOverlayItem;
			}
		}
		return null;
	}

	public boolean onSingleTapUp(float paramFloat1, float paramFloat2) {
		OverlayItem localOverlayItem = a(paramFloat1, paramFloat2);
		if (localOverlayItem != null) {
			onItemClick(localOverlayItem);
			return true;
		}
		return false;
	}

	public void onItemClick(OverlayItem overlayItem) {
	}

	private boolean isClicked(OverlayItem overlayItem, float x, float y) {
		if (!(overlayItem instanceof BaseOverlayItem))
			return false;
		
		BaseOverlayItem mBaseOverlayItem = (BaseOverlayItem) overlayItem;
		
		Boolean clicked = mBaseOverlayItem.isClicked(x, y); 
		return clicked;
	}
}
