package ru.oklogic.bus55.activity.map.controller;

import java.util.Collection;
import java.util.LinkedList;




import ru.oklogic.bus55.activity.map.BaseLayerController;
import ru.oklogic.bus55.activity.map.event.OnGeoDirectionClick;
import ru.oklogic.bus55.activity.map.event.OnGeoStationClick;
import ru.oklogic.bus55.activity.map.event.OnStationClick;
import ru.oklogic.bus55.activity.map.overlay.directions.DirectionBalloonItem;
import ru.oklogic.bus55.activity.map.overlay.directions.DirectionsOverlay;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.api.action.ParamNames;
import ru.oklogic.bus55.api.action.Parameters;
import ru.oklogic.bus55.map.balloon.BalloonController;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.GeoDirection;
import ru.oklogic.bus55.model.LatLong;

import ru.yandex.yandexmapkit.MapController;

public class DirectionLayerController extends BaseLayerController {
	DirectionsOverlay directionsOverlay;
	Collection<BaseOverlay> overlays;

	LatLong myLocationLatLong;
	LatLong selectedLocationLatLong;

	Collection<GeoDirection> myLocationDirections;
	Collection<GeoDirection> selectedLocationDirections;

	OnStationClick onShowPredictClick;
	OnGeoStationClick onRouteStartClick;
	OnGeoStationClick onRouteEndClick;

	public DirectionLayerController(MapController mMapController,
			BalloonController balloonController) {
		super(mMapController, balloonController);
		directionsOverlay = new DirectionsOverlay(mMapController);
		directionsOverlay.setOnDirectionClick(new OnGeoDirectionClick() {
			public void onClick(GeoDirection direction) {
				showDirectionBalloon(direction);
			}
		});
		myLocationDirections = new LinkedList<GeoDirection>();
		myLocationLatLong = new LatLong(0, 0);
		selectedLocationDirections = new LinkedList<GeoDirection>();
		overlays = new LinkedList<BaseOverlay>();
		overlays.add(directionsOverlay);
	}

	/**
	 * @param direction
	 */
	private void showDirectionBalloon(GeoDirection direction) {
		DirectionBalloonItem balloon = new DirectionBalloonItem(direction,
				getMapController().getContext());
		balloon.setOnPredictDirectionClick(new OnGeoDirectionClick() {
			public void onClick(GeoDirection direction) {
				onShowPredictClick.onClick(direction.getStation(),
						direction.getNextStation());
			}
		});
		balloon.setOnRouteStartClick(new OnGeoDirectionClick() {
			@Override
			public void onClick(GeoDirection direction) {
				onRouteStartClick.onClick(direction.getStation());
			}
		});
		balloon.setOnRouteEndClick(new OnGeoDirectionClick() {
			@Override
			public void onClick(GeoDirection direction) {
				onRouteEndClick.onClick(direction.getStation());
			}
		});
		getBalloonController().show(balloon);
	}

	public void selectLocation(LatLong latLong) {
		selectedLocationLatLong = latLong;
		Parameters params = new Parameters();
		params.put(ParamNames.LOCATION, latLong);
		Bus55API.getDirectionsGeoLoc(params,
				new Callback<Collection<GeoDirection>>() {
					public void run(Collection<GeoDirection> data) {
						onSelectedLocationStationLoaded(data);
					}
				}, getMapController().getContext());
	}

	public void setMyLocation(LatLong latLong) {
		Parameters params = new Parameters();
		params.put(ParamNames.LOCATION, latLong);
		Bus55API.getDirectionsGeoLoc(params,
				new Callback<Collection<GeoDirection>>() {
					public void run(Collection<GeoDirection> data) {
						onMyLocationStationLoaded(data);
					}
				}, getMapController().getContext());
	}

	private void onSelectedLocationStationLoaded(Collection<GeoDirection> data) {
		if (isNewSet(selectedLocationDirections, data)) {
			selectedLocationDirections = data;
			updateDirections();
		}
	}

	private void onMyLocationStationLoaded(Collection<GeoDirection> data) {
		if (isNewSet(myLocationDirections, data)) {
			myLocationDirections = data;
			updateDirections();
		}
	}

	private Boolean isNewSet(Collection<GeoDirection> list1,
			Collection<GeoDirection> list2) {
		for (GeoDirection direction1 : list1) {
			Boolean found = false;
			for (GeoDirection direction2 : list2) {
				if (direction1.getId().equals(direction2.getId())) {
					found = true;
					break;
				}
			}
			if (found == false) {
				return true;
			}
		}
		for (GeoDirection direction1 : list2) {
			Boolean found = false;
			for (GeoDirection direction2 : list1) {
				if (direction1.getId().equals(direction2.getId())) {
					found = true;
					break;
				}
			}
			if (found == false) {
				return true;
			}
		}
		return false;
	}

	public void setOnShowPredictClick(OnStationClick onShowPredictClick) {
		this.onShowPredictClick = onShowPredictClick;
	}

	public void setOnRouteStartClick(OnGeoStationClick callback) {
		this.onRouteStartClick = callback;
	}

	public void setOnRouteEndClick(OnGeoStationClick callback) {
		this.onRouteEndClick = callback;
	}

	private void updateDirections() {
		Collection<GeoDirection> directions = new LinkedList<GeoDirection>(
				myLocationDirections);
		directions.addAll(selectedLocationDirections);
		directionsOverlay.setGeoDirections(directions);
	}

	@Override
	public Collection<BaseOverlay> getOverlays() {
		return overlays;
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

}
