package ru.oklogic.bus55.adapter;

import java.util.LinkedList;
import java.util.List;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.model.Route;
import ru.oklogic.bus55.util.UtilStringBuilder;
import android.app.Activity;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class RouteListAdapter extends ArrayAdapter<Route> {
	final List<Route> items;
	final Activity context;
	
	Filter filter;
	
    public Route getByPosition(int position)
    {
        return items.get(position);
    }	

	public RouteListAdapter(Activity context, List<Route> items) {
		super(context, R.layout.listitem_route, new LinkedList<Route>(items));
		this.context = context;
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflator = context.getLayoutInflater();
		View view = inflator.inflate(R.layout.listitem_route, parent, false);
		Route route = getByPosition(position);

		TextView routeName = (TextView) view.findViewById(R.id.route_list_item_name);
		StringBuilder routeNameBuilder = new StringBuilder();
		routeNameBuilder.append(route.getName());
		routeNameBuilder.append(" ");
		Resources resource = getContext().getResources();
		String routeType = resource.getString(route.getType().getResourceIndex());
		routeNameBuilder.append(routeType);
		routeName.setText(routeNameBuilder.toString());				

		TextView routeDescription = (TextView) view.findViewById(R.id.route_list_item_description);

		routeDescription.setText(UtilStringBuilder.GetRouteDescription(
				route, getContext().getResources()));

		return view;
	}
}