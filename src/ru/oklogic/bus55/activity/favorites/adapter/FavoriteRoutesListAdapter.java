package ru.oklogic.bus55.activity.favorites.adapter;

import java.util.List;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.favorites.OnFavoriteRouteClick;
import ru.oklogic.bus55.activity.favorites.OnFavoriteRouteToggled;
import ru.oklogic.bus55.model.Route;
import ru.oklogic.bus55.util.UtilStringBuilder;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

public class FavoriteRoutesListAdapter extends ArrayAdapter<Route> {
	final List<Route> items;
	final Activity context;

	private OnFavoriteRouteToggled onFavoriteRouteToggledListener;
	private OnFavoriteRouteClick onFavoriteRouteClickListener;

	public Route getByPosition(int position) {
		return items.get(position);
	}

	public FavoriteRoutesListAdapter(Activity context, List<Route> items) {
		super(context, R.layout.listitem_route, items);
		this.context = context;
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflator = context.getLayoutInflater();
		View view = inflator.inflate(R.layout.listitem_favorite_route, parent, false);

		final Route route = getByPosition(position);

		TextView routeName = (TextView) view.findViewById(R.id.txt_route_name);
		routeName.setText(UtilStringBuilder.GetRouteName(route, context.getResources()));
		TextView routeDescription = (TextView) view.findViewById(R.id.txt_route_description);
		routeDescription.setText(UtilStringBuilder.GetRouteDescription(route, context.getResources()));

		ToggleButton toggleFavoriteButton = (ToggleButton) view
				.findViewById(R.id.btn_toggle_favorite);
		toggleFavoriteButton.setChecked(true);

		toggleFavoriteButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				onFavoriteToggled(route, isChecked);
			}
		});

		view.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				onFavoriteClick(route);
			}
		});

		return view;
	}

	public void setOnFavoriteRouteClickListener(
			OnFavoriteRouteClick onFavoriteRouteClickListener) {
		this.onFavoriteRouteClickListener = onFavoriteRouteClickListener;
	}

	private void onFavoriteClick(Route route) {
		onFavoriteRouteClickListener.onClick(route);
	}

	public void setOnFavoriteRouteToggledListener(
			OnFavoriteRouteToggled onFavoriteRouteToggledListener) {
		this.onFavoriteRouteToggledListener = onFavoriteRouteToggledListener;
	}

	private void onFavoriteToggled(Route route, Boolean isChecked) {
		onFavoriteRouteToggledListener.onToggle(route, isChecked);
	}
}
