package ru.oklogic.bus55.api.action;

import java.util.LinkedList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.oklogic.bus55.model.GeoVehicle;
import ru.oklogic.bus55.model.LatLong;
import ru.oklogic.bus55.model.Route;

public class GetGeoRouteVehicleListAction extends BaseAction<Collection<GeoVehicle>> {
	@Override
	protected String getUrl(Parameters params) {
		Route route = (Route) params.get(ParamNames.ROUTE);

		return "http://bus.admomsk.ru/index.php/getroute/getbus/"
				+ route.getId();
	}

	@Override
	protected Collection<GeoVehicle> parse(String response, Parameters params) {
		Route route = (Route) params.get(ParamNames.ROUTE);
		LinkedList<GeoVehicle> vehicles = new LinkedList<GeoVehicle>();

		try {
				JSONObject json = new JSONObject(response);
				JSONArray vehiclesArray = json.getJSONArray("vehicles");

				for (int i = 0; i < vehiclesArray.length(); ++i) {
					JSONObject vehicle = vehiclesArray.getJSONObject(i);
					JSONArray coordinate = vehicle.getJSONArray("coordinates");

					Double latitude = coordinate.getDouble(1);
					Double longitude = coordinate.getDouble(0);

					LatLong latLong = new LatLong(latitude, longitude);

					Integer id = vehicle.getInt("id");
					Integer course = vehicle.getInt("course");
					String info = vehicle.getString("info");

					GeoVehicle geoVehicle = new GeoVehicle(id, latLong,
							course.floatValue(), route, info);

					vehicles.add(geoVehicle);
				}
		} catch (Exception e) {
		}
		
		return vehicles;
	}

}
