package ru.oklogic.bus55.activity.map.overlay.busroute;

import java.util.List;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.map.event.OnGeoStationClick;
import ru.oklogic.bus55.activity.map.overlay.busroute.station.StationOverlayItem;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.GeoRoute;
import ru.oklogic.bus55.model.GeoStation;
import ru.oklogic.bus55.model.Route;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BusRouteStationsOverlay extends BaseOverlay {
	GeoRoute geoRoute;

	OnGeoStationClick onStationClick;

	public BusRouteStationsOverlay(MapController mMapController) {
		super(mMapController);
	}

	public Route getGeoRoute() {
		return geoRoute;
	}

	public void clearGeoRoute() {
		clearOverlayItems();
		this.geoRoute = null;
	}

	public void setOnStationClick(OnGeoStationClick onStationClick) {
		this.onStationClick = onStationClick;
	}

	public void setGeoRoute(GeoRoute geoRoute) {
		clearGeoRoute();
		this.geoRoute = geoRoute;
		updateStations(geoRoute.getStations());
	}

	public void onItemClick(OverlayItem overlayItem) {
		if (onStationClick == null)
			return;
		StationOverlayItem stationOverlayItem = (StationOverlayItem) overlayItem;
		onStationClick.onClick(stationOverlayItem.getGeoStation());
	}

	private void updateStations(List<GeoStation> stations) {
		for (GeoStation station : stations) {
			// Load required resources
			Resources res = getResources();
			Bitmap busStationBitmap = BitmapFactory.decodeResource(res,
					R.drawable.ic_map_bus_station);

			StationOverlayItem busStationOverlayItem = new StationOverlayItem(
					station, busStationBitmap);

			addOverlayItem(busStationOverlayItem);
		}
	}
}
