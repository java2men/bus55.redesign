package ru.oklogic.bus55.activity.menu;

import com.darvds.ribbonmenu.RibbonMenuView;
import com.darvds.ribbonmenu.iRibbonMenuCallback;
import com.darvds.ribbonmenu.iRibbonMenuVisibilityChangeCallback;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.favorites.FavoriteDirectionsActivity;
import ru.oklogic.bus55.activity.map.MainMapView;
import ru.oklogic.bus55.activity.routes.RouteGroupListActivity;
import ru.oklogic.bus55.activity.stations.StationActivity;

import android.app.Activity;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class RibbonMenuController implements iRibbonMenuCallback {
	RibbonMenuView rbmView;
	
	public void init(Activity activity) {
		if (rbmView != null)
			return;

		rbmView = new RibbonMenuView(activity);
		rbmView.setMenuClickCallback(this);
		rbmView.setMenuItems(R.menu.home_menu);

		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.MATCH_PARENT,
				FrameLayout.LayoutParams.MATCH_PARENT);

		ViewGroup content = (ViewGroup) activity
				.findViewById(R.id.inner_content);
		content.addView(rbmView, layoutParams);
	}

	public Boolean isShown() {
		return rbmView.isMenuVisible();
	}

	public void show() {
		rbmView.showMenu();
		rbmView.bringToFront();
	}

	public void hide() {
		rbmView.hideMenu();
	}

	public void toggle() {
		rbmView.toggleMenu();
		rbmView.bringToFront();
	}

	public void setHideMenuCallback(iRibbonMenuVisibilityChangeCallback callback) {
		rbmView.setHideMenuCallback(callback);
	}

	public void setShowMenuCallback(iRibbonMenuVisibilityChangeCallback callback) {
		rbmView.setShowMenuCallback(callback);
	}

	@Override
	public void RibbonMenuItemClick(int itemId) {
		switch (itemId) {
		case R.id.menu_home_stations:
			gotoActivity(getStationViewClass());
			break;
		case R.id.menu_home_routes:
			gotoActivity(getRouteGroupViewClass());
			break;
		case R.id.menu_home_map:
			gotoActivity(getMapViewClass());
			break;
		case R.id.menu_home_favorites:
			gotoActivity(getFavoritesViewClass());
			break;
		}
	}

	private void gotoActivity(Class<?> cls) {
		if (this.getClass().equals(cls))
			return;

		Intent intent = new Intent(
				rbmView.getContext().getApplicationContext(), cls);
		rbmView.getContext().startActivity(intent);
	}

	private Class<?> getStationViewClass() {
		return StationActivity.class;
	}

	private Class<?> getRouteGroupViewClass() {
		return RouteGroupListActivity.class;
	}

	private Class<?> getMapViewClass() {
		return MainMapView.class;
	}

	private Class<?> getFavoritesViewClass() {
		return FavoriteDirectionsActivity.class;
	}
}
