package ru.oklogic.bus55.activity.favorites.adapter;

import java.util.List;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.favorites.OnFavoriteDirectionClick;
import ru.oklogic.bus55.activity.favorites.OnFavoriteDirectionToggled;
import ru.oklogic.bus55.model.Direction;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

public class FavoriteDirectionsListAdapter extends ArrayAdapter<Direction> {
	final List<Direction> items;
	final Activity context;

	private OnFavoriteDirectionToggled onFavoriteDirectionToggledListener;
	private OnFavoriteDirectionClick onFavoriteDirectionClickListener;

	public Direction getByPosition(int position) {
		return items.get(position);
	}

	public FavoriteDirectionsListAdapter(Activity context, List<Direction> items) {
		super(context, R.layout.listitem_direction, items);
		this.context = context;
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflator = context.getLayoutInflater();
		View view = inflator.inflate(R.layout.listitem_favorite_direction, parent, false);

		final Direction direction = getByPosition(position);

		TextView stationName = (TextView) view.findViewById(R.id.station_name);
		stationName.setText(direction.getStation().getName());
		TextView nextStationName = (TextView) view.findViewById(R.id.next_station_name);
		nextStationName.setText(direction.getNextStation().getName());

		ToggleButton toggleFavoriteButton = (ToggleButton) view
				.findViewById(R.id.btn_toggle_favorite);
		toggleFavoriteButton.setChecked(true);

		toggleFavoriteButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				onFavoriteToggled(direction, isChecked);
			}
		});

		view.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				onFavoriteClick(direction);
			}
		});

		return view;
	}

	public void setOnFavoriteDirectionClickListener(
			OnFavoriteDirectionClick onFavoriteDirectionClickListener) {
		this.onFavoriteDirectionClickListener = onFavoriteDirectionClickListener;
	}

	private void onFavoriteClick(Direction direction) {
		onFavoriteDirectionClickListener.onClick(direction);
	}

	public void setOnFavoriteDirectionToggledListener(
			OnFavoriteDirectionToggled onFavoriteDirectionToggledListener) {
		this.onFavoriteDirectionToggledListener = onFavoriteDirectionToggledListener;
	}

	private void onFavoriteToggled(Direction direction, Boolean isChecked) {
		onFavoriteDirectionToggledListener.onToggle(direction, isChecked);
	}
}
