package ru.oklogic.bus55.activity.map.controller;

import java.util.Collection;
import java.util.LinkedList;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import ru.oklogic.bus55.activity.map.BaseLayerController;
import ru.oklogic.bus55.activity.map.MainMapView;
import ru.oklogic.bus55.map.balloon.BalloonController;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.LatLong;
import ru.yandex.yandexmapkit.MapController;

public class LocationController extends BaseLayerController {

	MainMapView mainMapView;
	Collection<BaseLayerController> controllers;

	LocationListener networkLocationListener;
	LocationListener gpsLocationListener;
	LocationManager locationManager;

	Boolean isGPSAvailable = false;

	public LocationController(MainMapView mainMapView,
			MapController mMapController,
			Collection<BaseLayerController> controllers,
			BalloonController balloonController) {

		super(mMapController, balloonController);

		this.mainMapView = mainMapView;
		this.controllers = controllers;

		initLocationListener();
	}

	@Override
	public Collection<BaseOverlay> getOverlays() {
		return new LinkedList<BaseOverlay>();
	}

	@Override
	public void pause() {
		locationManager.removeUpdates(networkLocationListener);
		locationManager.removeUpdates(gpsLocationListener);
	}

	@Override
	public void resume() {
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 10000L, 1.0F,
				networkLocationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				500L, 1.0F, gpsLocationListener);
	}

	private void initLocationListener() {
		// Acquire a reference to the system Location Manager
		locationManager = (LocationManager) mainMapView
				.getSystemService(Context.LOCATION_SERVICE);

		// Define a listener that responds to location updates
		networkLocationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				if (isGPSAvailable)
					return;

				updateLocation(new LatLong(location.getLatitude(),
						location.getLongitude()));
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};

		gpsLocationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				isGPSAvailable = true;
				updateLocation(new LatLong(location.getLatitude(),
						location.getLongitude()));
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				if (status == LocationProvider.TEMPORARILY_UNAVAILABLE)
					isGPSAvailable = false;
				if (status == LocationProvider.OUT_OF_SERVICE)
					isGPSAvailable = false;
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
				isGPSAvailable = false;
			}
		};
	}

	private void updateLocation(LatLong latLong) {
		for (BaseLayerController controller : controllers) {
			controller.setMyLocation(latLong);
		}
	}
}
