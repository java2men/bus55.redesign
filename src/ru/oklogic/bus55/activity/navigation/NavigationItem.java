package ru.oklogic.bus55.activity.navigation;

public abstract class NavigationItem {
	public abstract int getIcon();
	public abstract String getTitle();
	public abstract Class<?> getActivityClass(); 
	
	@Override
	public String toString() {
		return getTitle();
	}
}
