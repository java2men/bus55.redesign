package ru.oklogic.bus55.activity.stations;

import java.util.LinkedList;
import java.util.Collection;

import com.androidquery.AQuery;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.HomeSherlockActivity;
import ru.oklogic.bus55.adapter.StationListAdapter;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.api.action.ParamNames;
import ru.oklogic.bus55.api.action.Parameters;
import ru.oklogic.bus55.model.Station;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class StationActivity extends HomeSherlockActivity {
	StationListAdapter stationListAdapter;
	ListView stationList;
	AQuery searchStationQuery = null;
	Handler updateHandler;
	
	static Integer SEARCH_MESSAGE = 1;

	protected String getActivityTitle() {
		return getResources().getString(R.string.station_title);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_list_stations);

		TextView text = (TextView) findViewById(R.id.search_station_field);
		text.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
		        updateDirections(s.toString());
			}

			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			public void afterTextChanged(Editable arg0) {
			}
		});

		stationList = (ListView) findViewById(R.id.stations_list);
		stationListAdapter = new StationListAdapter(this,
				new LinkedList<Station>());
		stationList.setAdapter(stationListAdapter);

		stationList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				gotoSelectDirection(view,
						stationListAdapter.getByPosition(position));
			}
		});
		
		updateHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				Parameters params = new Parameters();
				String search = (String) msg.obj;
				params.put(ParamNames.SEARCH, search);
				
				searchStationQuery = Bus55API.searchStationList(params, new Callback<Collection<Station>>() {
					public void run(Collection<Station> data) {
						searchStationQuery = null;
						hideLoading();
						loadData(data);
					}
				}, StationActivity.this);
				return true;
			}
		});
	}

	private void updateDirections(String search) {
		if (searchStationQuery != null) {
			searchStationQuery.dismiss();
			searchStationQuery = null;
		}

		updateHandler.removeMessages(0);
		Message m = new Message();
		m.obj = search;
		updateHandler.sendMessageDelayed(m, 1000);
	}

	private void loadData(Collection<Station> data) {
		stationListAdapter = new StationListAdapter(this,
				new LinkedList<Station>(data));
		stationList.setAdapter(stationListAdapter);
	}

	public void gotoSelectDirection(View view, Station station) {
		Intent gotoDirectionViewActivity = new Intent(
				this.getApplicationContext(), StationDirectionActivity.class);
		gotoDirectionViewActivity.putExtra("station", station);
		this.startActivity(gotoDirectionViewActivity);
	}
}
