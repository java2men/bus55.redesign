package ru.oklogic.bus55.activity.navigation;

import android.content.Context;
import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.map.MainMapView;

public class NavigationMapItem extends NavigationItem {
	Context context;	
	public NavigationMapItem(Context context) {
		this.context = context;
	}
	
	public int getIcon() {
		return R.drawable.btn_main_globe;
	}
	public Class<?> getActivityClass() {
		return MainMapView.class;
	}

	@Override
	public String getTitle() {
		return context.getString(R.string.map_button);
	}
}
