package ru.oklogic.bus55.activity.map.overlay.busroute;

import java.util.Iterator;
import java.util.List;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.map.overlay.busroute.route.RouteOverlayItem;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.map.overlay.item.CustomRenderer;
import ru.oklogic.bus55.model.GeoRoute;
import ru.oklogic.bus55.model.LatLong;
import ru.oklogic.bus55.model.Route;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.ScreenPoint;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BusRouteOverlay extends BaseOverlay {
	GeoRoute geoRoute;

	public BusRouteOverlay(MapController mMapController) {
		super(mMapController);

		setIRender(new CustomRenderer());
	}

	public Route getGeoRoute() {
		return geoRoute;
	}

	public void setGeoRoute(GeoRoute geoRoute) {
		this.geoRoute = geoRoute;

		clearOverlayItems();
		updatePath(geoRoute.getPath());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List prepareDraw() {
		this.b.clear();
		if (!isVisible())
			return this.b;
		Iterator localIterator = getOverlayItems().iterator();
		while (localIterator.hasNext()) {
			OverlayItem localOverlayItem = (OverlayItem) localIterator.next();
			try {
				if (!localOverlayItem.isVisible())
					continue;
				ScreenPoint localScreenPoint = this.c
						.getScreenPoint(localOverlayItem.getPoint());
				localOverlayItem.setScreenPoint(localScreenPoint);
				this.b.add(localOverlayItem);
			} catch (Throwable localThrowable) {
			}
		}
		return getPrepareDrawList();
	}

	private void updatePath(List<LatLong> path) {
		if (path.size() == 0)
			return;

		Resources res = getResources();

		Bitmap empty = BitmapFactory.decodeResource(res,
				R.drawable.ic_empty);
		
		addOverlayItem(new RouteOverlayItem(path.get(0).toGeoPoint(), empty,
				path, getMapController()));
	}
}
