package ru.oklogic.bus55.util;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import ru.oklogic.bus55.model.Direction;
import ru.oklogic.bus55.model.Route;
import android.content.res.Resources;


public class UtilStringBuilder {
	public static String GetPredictName(
			Direction direction, Resources r){
		StringBuilder builder = new StringBuilder();
		builder.append(direction.getStation().getName());
		return builder.toString();
	}
	public static String GetRouteDescription(
			Route route, Resources r){
		return route.getDescription();
	}
	public static String GetRouteName(
			Route route, Resources r){
		StringBuilder builder = new StringBuilder();
		builder.append(route.getName());
		builder.append(" ");
		builder.append(r.getString(route.getType().getResourceIndex()));
		return builder.toString();
	}
	
	public static String GetContentText(Node node){
		NodeList list = node.getChildNodes(); 
		StringBuilder contentBuilder = new StringBuilder();
		for (Integer i = 0; i < list.getLength(); ++i)
		{
			if (list.item(i) instanceof Text)
			{
				Text textItem = (Text) list.item(i);
				contentBuilder.append(textItem.getData());
			}
		}
		return contentBuilder.toString();
	}
}
