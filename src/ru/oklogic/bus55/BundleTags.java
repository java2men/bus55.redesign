package ru.oklogic.bus55;

public class BundleTags {
	final public static String ROUTE = "route";
	final public static String DIRECTION = "direction";
	final public static String LOCATION = "location";
	final public static String LATITUDE = "latitude";
	final public static String LONGITUDE = "longitude";
	final public static String ZOOM = "zoom";
	final public static String JAMS = "jams";
}
