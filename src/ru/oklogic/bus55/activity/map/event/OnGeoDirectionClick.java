package ru.oklogic.bus55.activity.map.event;

import ru.oklogic.bus55.model.GeoDirection;

public interface OnGeoDirectionClick {
	void onClick(GeoDirection station);
}
