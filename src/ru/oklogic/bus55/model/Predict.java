package ru.oklogic.bus55.model;

import java.io.Serializable;

public class Predict implements Serializable{
	private static final long serialVersionUID = -2388510752165130507L;
	
	Route route;
	String time;
	
	public Predict(){}
	
	public Predict(Route route, String time){
		this.route = route;
		this.time = time;
	}
	
	public Route getRoute(){
		return this.route;
	}
	
	public void setRoute(Route route){
		this.route = route;
	}
	
	public String getDate(){
		return this.time;
	}
	
	public void setDate(String time){
		this.time = time;
	}
}
