package ru.oklogic.bus55.activity.map.overlay.busroute.station;

import java.util.List;

import ru.oklogic.bus55.map.overlay.item.BaseOverlayItem;
import ru.oklogic.bus55.model.GeoStation;
import ru.oklogic.bus55.model.LatLong;
import ru.yandex.yandexmapkit.MapController;
import android.graphics.Bitmap;

public class StationOverlayItem extends BaseOverlayItem {
	List<LatLong> routePath;
	MapController mapController;
	GeoStation geoStation;

	public byte getPriority() {
		return 3;
	}
	
	protected boolean isClickable() {
		return true;
	}

	public StationOverlayItem(GeoStation geoStation, Bitmap bitmap) {
		super(geoStation.getLatLong().toGeoPoint(), bitmap);
		
		this.geoStation = geoStation;
	}
	
	public GeoStation getGeoStation() {
		return geoStation;
	}
}
