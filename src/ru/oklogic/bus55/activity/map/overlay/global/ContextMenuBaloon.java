package ru.oklogic.bus55.activity.map.overlay.global;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.map.event.OnLatLongClick;
import ru.oklogic.bus55.map.balloon.item.BaseBalloonItem;
import ru.oklogic.bus55.model.LatLong;
import ru.yandex.yandexmapkit.utils.GeoPoint;

public class ContextMenuBaloon extends BaseBalloonItem {
	LinearLayout internalLayout;
	LatLong latLong;

	OnLatLongClick onShowStationsHereClickListener;
	OnLatLongClick onRouteStartClickListener;
	OnLatLongClick onRouteEndClickListener;

	public ContextMenuBaloon(LatLong latLong, Context context) {
		super(context, latLong.toGeoPoint());
		this.latLong = latLong;

		Resources resources = this.getResources();

		TextView showStations = new TextView(context);
		showStations.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		showStations.setText(resources.getString(R.string.show_stations_here));
		showStations.setBackgroundResource(R.drawable.bg_map_balloon_clickable);
		
		TextView findRouteStart = new TextView(context);
		findRouteStart.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		findRouteStart.setText(resources.getString(R.string.txt_findroute_start));
		findRouteStart.setBackgroundResource(R.drawable.bg_map_balloon_clickable);

		TextView findRouteEnd = new TextView(context);
		findRouteEnd.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		findRouteEnd.setText(resources.getString(R.string.txt_findroute_end));
		findRouteEnd.setBackgroundResource(R.drawable.bg_map_balloon_clickable);

		try {
			XmlResourceParser parser = getResources().getXml(R.drawable.text_color);
			ColorStateList colors = ColorStateList.createFromXml(getResources(), parser);
			showStations.setTextColor(colors);
			findRouteStart.setTextColor(colors);
			findRouteEnd.setTextColor(colors);
		} catch (Exception e) {
		}

		internalLayout = new LinearLayout(context);
		internalLayout.setOrientation(LinearLayout.VERTICAL);
		internalLayout.addView(showStations);
//		internalLayout.addView(findRouteStart);
//		internalLayout.addView(findRouteEnd);

		showStations.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onShowStationHereClick();
			}
		});
		findRouteStart.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onRouteStartClick();
			}
		});
		findRouteEnd.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onRouteEndClick();
			}
		});
		
		this.addView(internalLayout);
		addCloseButton();
	}

	public void setOnShowStationsHereClickListener(OnLatLongClick onClickListener) {
		onShowStationsHereClickListener = onClickListener;
	}

	public void setOnRouteStartClickListener(OnLatLongClick onClickListener) {
		onRouteStartClickListener = onClickListener;
	}

	public void setOnRouteEndClickListener(OnLatLongClick onClickListener) {
		onRouteEndClickListener = onClickListener;
	}

	private void onShowStationHereClick() {
		onShowStationsHereClickListener.onClick(latLong);
		hide();
	}

	private void onRouteStartClick() {
		onRouteStartClickListener.onClick(latLong);
		hide();
	}

	private void onRouteEndClick() {
		onRouteEndClickListener.onClick(latLong);
		hide();
	}

	public GeoPoint getGeoPoint() {
		return latLong.toGeoPoint();
	}

	private void hide() {
		setVisibility(INVISIBLE);
	}

}
