package ru.oklogic.bus55;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.favorites.FavoriteDirectionsActivity;
import ru.oklogic.bus55.activity.map.MainMapView;
import ru.oklogic.bus55.activity.routes.RouteGroupListActivity;
import ru.oklogic.bus55.activity.stations.StationActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.actionbarsherlock.app.SherlockActivity;

public class Main extends SherlockActivity {

	protected static final int ITEM_ID_HOME = 0;
	public static int THEME = R.style.Theme_Sherlock_Light;

	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Sherlock_Light_DarkActionBar);
		super.onCreate(savedInstanceState);
		getSupportActionBar().setBackgroundDrawable(
				getResources().getDrawable(R.drawable.bg_tbar));

		setContentView(R.layout.act_home);
		getSupportActionBar().setIcon(R.drawable.ic_bus);
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (featureId) {
		case ITEM_ID_HOME:
			onHomeClick();
			return true;
		default:
			break;
		}

		return super.onMenuItemSelected(featureId, item);
	}

	public void onHomeClick() {
	}

	public void gotoStationView(View view) {
		Intent intent = new Intent(
				this.getApplicationContext(), StationActivity.class);
		startActivity(intent);
	}
	
	public void gotoRouteGroupView(View view) {
		Intent intent = new Intent(
				this.getApplicationContext(), RouteGroupListActivity.class);
		startActivity(intent);
	}
	
	public void gotoMapView(View view) {
		Intent intent = new Intent(
				this.getApplicationContext(), MainMapView.class);
		startActivity(intent);
	}
	
	public void gotoFavorites(View view) {
		Intent intent = new Intent(
				this.getApplicationContext(), FavoriteDirectionsActivity.class);
		startActivity(intent);
	}
}
