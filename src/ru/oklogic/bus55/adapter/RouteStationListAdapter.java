package ru.oklogic.bus55.adapter;

import java.util.LinkedList;
import java.util.List;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.model.Station;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class RouteStationListAdapter extends ArrayAdapter<Station> {
	final List<Station> items;
	final Activity context;
	List<Station> filtered;
	
	Filter filter;
	
    public Station getByPosition(int position)
    {
    	if (position >= filtered.size())
    		return null;
        return filtered.get(position);
    }	

	public RouteStationListAdapter(Activity context, List<Station> items) {
		super(context, R.layout.listitem_routestation, new LinkedList<Station>(items));
		this.context = context;
		this.filtered = items;
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflator = context.getLayoutInflater();
		View view = inflator.inflate(R.layout.listitem_routestation, parent, false);
		TextView text = (TextView) view.findViewById(R.id.station_list_item_name);

		text.setText(getByPosition(position).getName());
		return view;
	}
	
	@Override
	public Filter getFilter()
	{
		if(filter == null)
            filter = new StationsFilter();
        return filter;
	}
	
	private class StationsFilter extends Filter
    {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // NOTE: this function is *always* called from a background thread, and
            // not the UI thread.
            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if(constraint != null && constraint.toString().length() > 0)
            {
                List<Station> filt = new LinkedList<Station>();
                List<Station> lItems = new LinkedList<Station>();
                synchronized (this)
                {
                    lItems.addAll(items);
                }
                for(int i = 0, l = lItems.size(); i < l; i++)
                {
                    Station station = lItems.get(i);
                    String[] searchStrings = constraint.toString().split(" ");
                    
                    Boolean isSatisfy = true;
                    
                    for (Integer subIndex = 0; subIndex < searchStrings.length; ++subIndex)
                    {
                    	if(!station.getName().toLowerCase().contains(searchStrings[subIndex]))
                    	{
                    		isSatisfy = false;
                    		break;
                    	}
                    }
                    
                    if(isSatisfy){
                        filt.add(station);
                    }
                }
                result.count = filt.size();
                result.values = filt;
            }
            else
            {
                synchronized(this)
                {
                    result.values = new LinkedList<Station>(items);
                    result.count = items.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // NOTE: this function is *always* called from the UI thread.
            filtered = (List<Station>)results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = filtered.size(); i < l; i++)
                add(filtered.get(i));
            notifyDataSetInvalidated();
        }
    }
}
