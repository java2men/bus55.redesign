package ru.oklogic.bus55.api.action;

import java.util.LinkedList;
import java.util.Collection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ru.oklogic.bus55.model.Route;
import ru.oklogic.bus55.model.Station;
import android.util.Pair;

public class GetRouteStationsAction extends
		BaseAction<Pair<Collection<Station>, Collection<Station>>> {

	@Override
	protected long getExpireTime() {
		return 15*60*1000;
	}
	
	@Override
	protected String getUrl(Parameters params) {
		Route route = (Route) params.get(ParamNames.ROUTE);
		return "http://t.bus55.ru/index.php/app/get_stations/" + route.getId();
	}

	@Override
	protected Pair<Collection<Station>, Collection<Station>> parse(String response,
			Parameters params) {
		try {
			Document dom = parseXML(response);
			Pair<Collection<Station>, Collection<Station>> result = new Pair<Collection<Station>, Collection<Station>>(
					parseDirectionA(dom), parseDirectionB(dom));

			return result;
		} catch (Exception ex) {
		}

		return new Pair<Collection<Station>, Collection<Station>>(
				new LinkedList<Station>(), new LinkedList<Station>());
	}

	protected Collection<Station> parseDirectionA(Document dom) {
		Collection<Station> stations = new LinkedList<Station>();
		try {
			Element root = dom.getDocumentElement();
			NodeList stationNodes = root.getElementsByTagName("li");
			Integer counter = 1;
			for (; counter < stationNodes.getLength(); ++counter) {
				Element el = (Element) stationNodes.item(counter);
				if (el.getAttribute("class").equals("group")) {
					break;
				}
			}
			for (Integer index = 1; index < counter; ++index) {
				Station station = new Station();
				Element stationElement = (Element) stationNodes.item(index)
						.getChildNodes().item(0);

				String sStationId = stationElement.getAttribute("id");
				String[] sss = sStationId.split("_");
				String sStationNumberEx = sss[1];
				String stationName = stationElement.getChildNodes().item(0)
						.getNodeValue();

				station.setId(sStationNumberEx);
				station.setName(stationName);

				stations.add(station);
			}
		} catch (Exception e) {

		}
		return stations;
	}

	protected Collection<Station> parseDirectionB(Document dom) {
		Collection<Station> stations = new LinkedList<Station>();
		try {
			Element root = dom.getDocumentElement();
			NodeList stationNodes = root.getElementsByTagName("li");
			Integer counter = 1;
			for (; counter < stationNodes.getLength(); ++counter) {
				Element el = (Element) stationNodes.item(counter);
				if (el.getAttribute("class").equals("group")) {
					break;
				}
			}
			for (Integer index = counter + 1; index < stationNodes.getLength(); ++index) {
				Station station = new Station();
				Element stationElement = (Element) stationNodes.item(index)
						.getChildNodes().item(0);

				String sStationId = stationElement.getAttribute("id");
				String[] sss = sStationId.split("_");
				String sStationNumberEx = sss[1];
				String stationName = stationElement.getChildNodes().item(0)
						.getNodeValue();

				station.setId(sStationNumberEx);
				station.setName(stationName);

				stations.add(station);
			}
		} catch (Exception e) {

		}
		return stations;
	}
}
