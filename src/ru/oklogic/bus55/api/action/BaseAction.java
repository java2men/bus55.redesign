package ru.oklogic.bus55.api.action;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ru.oklogic.bus55.api.Callback;
import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

abstract public class BaseAction<T> {

	abstract protected String getUrl(Parameters params);

	abstract protected T parse(String response, Parameters params);
	
	protected long getExpireTime() {
		return -1;
	}

	final protected Document parseXML(String content)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputStream contentStream = new ByteArrayInputStream(content.getBytes());
		return builder.parse(contentStream);
	}

	public AQuery call(final Parameters params, final Callback<T> callback,
			Context context) {
		AQuery aQuery = new AQuery(context);

		return aQuery.ajax(getUrl(params), String.class, getExpireTime(),
				new AjaxCallback<String>() {

					@Override
					public void callback(String url, String response,
							AjaxStatus status) {
						if (response != null) {
							callback.run(parse(response, params));
						}
					}
				});
	}
}
