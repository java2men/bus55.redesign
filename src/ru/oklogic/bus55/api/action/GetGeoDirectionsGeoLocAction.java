package ru.oklogic.bus55.api.action;

import java.util.Collection;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.oklogic.bus55.model.GeoDirection;
import ru.oklogic.bus55.model.GeoStation;
import ru.oklogic.bus55.model.LatLong;

public class GetGeoDirectionsGeoLocAction extends BaseAction<Collection<GeoDirection>> {
	@Override
	protected long getExpireTime() {
		return 15 * 60 * 1000;
	}

	@Override
	protected String getUrl(Parameters params) {
		LatLong latLong = (LatLong) params.get(ParamNames.LOCATION);

		return "http://t.bus55.ru/index.php/app/get_stations_geoloc_json/" + latLong.getLatitude()
				+ "/" + latLong.getLongitude();
	}

	@Override
	protected Collection<GeoDirection> parse(String response, Parameters params) {
		Collection<GeoDirection> directions = new LinkedList<GeoDirection>();
		try {
			JSONArray dirList = new JSONArray(response);
			int dirListLength = dirList.length();
			for (int index = 0; index < dirListLength; ++index) {
				try {
					GeoDirection direction = new GeoDirection();
					GeoStation station = new GeoStation();
					GeoStation nextStation = new GeoStation();

					JSONObject directionJson = dirList.getJSONObject(index);
					JSONObject nextStationJson = directionJson.getJSONObject("dir");

					station.setId(nextStationJson.getString("id_station_1"));
					station.setLatLong(new LatLong(Double.parseDouble(directionJson
							.getString("lat")), Double.parseDouble(directionJson.getString("lon"))));
					station.setName(directionJson.getString("name"));
					direction.setStation(station);
					direction.setId(station.getId());

					nextStation.setId(nextStationJson.getString("id_station_2"));
					nextStation.setName(nextStationJson.getString("station_name_2"));
					direction.setNextStation(nextStation);

					String description = nextStationJson.getString("routes");
					description = description.substring(0, description.length() - 2);
					direction.setDescription(description);
					directions.add(direction);
				} catch (JSONException e) {
				}
			}
		} catch (JSONException e) {
		}
		return directions;
	}

}
