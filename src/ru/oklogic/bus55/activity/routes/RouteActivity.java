package ru.oklogic.bus55.activity.routes;

import java.util.Collection;
import java.util.LinkedList;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.BundleTags;
import ru.oklogic.bus55.activity.SecondarySherlockActivity;
import ru.oklogic.bus55.activity.map.MainMapView;
import ru.oklogic.bus55.activity.stations.PredictActivity;
import ru.oklogic.bus55.adapter.RouteStationListAdapter;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.api.action.ParamNames;
import ru.oklogic.bus55.api.action.Parameters;
import ru.oklogic.bus55.db.service.FavoriteRoutesService;
import ru.oklogic.bus55.model.Route;
import ru.oklogic.bus55.model.Station;
import ru.oklogic.bus55.model.Util;
import ru.oklogic.bus55.util.UtilStringBuilder;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;

public class RouteActivity extends SecondarySherlockActivity {
	final Integer ACTION_STAR = 1;
	final Integer ACTION_MAP = 2;

	Route route;

	ListView firstStationList;
	ListView secondStationList;

	RouteStationListAdapter secondStationsListAdapter;
	RouteStationListAdapter firstStationsListAdapter;

	AQuery updateRouteAjax;

	FavoriteRoutesService favoritesService;

	protected String getActivityTitle() {
		return UtilStringBuilder.GetRouteName(route, getResources());
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		route = (Route) getIntent().getExtras().getSerializable(
				BundleTags.ROUTE);

		super.onCreate(savedInstanceState);

		getSupportActionBar().setSubtitle(
				UtilStringBuilder.GetRouteDescription(route, getResources()));

		favoritesService = new FavoriteRoutesService(this);

		setContentView(R.layout.act_list_route_station);
		this.getSupportActionBar().setTitle(getActivityTitle());

		initLists();
		initTabs();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(Menu.NONE, ACTION_STAR, Menu.NONE, R.string.btn_favorite)
				.setIcon(
						favoritesService.isRouteFavorite(route) ? R.drawable.btn_actionbar_star_checked
								: R.drawable.btn_actionbar_star)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add(Menu.NONE, ACTION_MAP, Menu.NONE, R.string.btn_showonmap)
				.setIcon(R.drawable.btn_actionbar_map)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_ALWAYS);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == ACTION_MAP) {
			gotoMapView();
		} else if (id == ACTION_STAR) {
			toggleFavorites();
		} else {
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void initLists() {
		firstStationList = (ListView) findViewById(R.id.first_direction_station_list);
		firstStationList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Station firstStation = firstStationsListAdapter
						.getByPosition(position);
				Station nextStation = firstStationsListAdapter
						.getByPosition(position + 1);
				gotoPredictView(firstStation, nextStation);
			}
		});

		secondStationList = (ListView) findViewById(R.id.second_direction_station_list);
		secondStationList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Station firstStation = secondStationsListAdapter
						.getByPosition(position);
				Station nextStation = secondStationsListAdapter
						.getByPosition(position + 1);

				gotoPredictView(firstStation, nextStation);
			}
		});

		firstStationsListAdapter = new RouteStationListAdapter(this,
				new LinkedList<Station>());
		firstStationList.setAdapter(firstStationsListAdapter);
		secondStationsListAdapter = new RouteStationListAdapter(this,
				new LinkedList<Station>());
		secondStationList.setAdapter(secondStationsListAdapter);

		updateStations();
	}

	private void updateStations() {

		Parameters params = new Parameters();
		params.put(ParamNames.ROUTE, route);
		showLoading();

		updateRouteAjax = Bus55API.getRouteStationList(params,
				new Callback<Pair<Collection<Station>, Collection<Station>>>() {
					public void run(
							Pair<Collection<Station>, Collection<Station>> data) {
						hideLoading();
						firstStationsListAdapter.clear();
						secondStationsListAdapter.clear();
						loadData(data);
					}
				}, this);
	}

	private void loadData(Pair<Collection<Station>, Collection<Station>> data) {
		firstStationsListAdapter = new RouteStationListAdapter(this,
				new LinkedList<Station>(data.first));
		firstStationList.setAdapter(firstStationsListAdapter);
		secondStationsListAdapter = new RouteStationListAdapter(this,
				new LinkedList<Station>(data.second));
		secondStationList.setAdapter(secondStationsListAdapter);
	}

	public void onCancelLoading() {
		updateRouteAjax.clear();
	}

	private void initTabs() {
		TabHost tabs = (TabHost) this.findViewById(R.id.tabhost);
		tabs.setup();

		TextView txtTabInfo1 = createTabInfo();
		txtTabInfo1.setText(R.string.direction_a);
		TabSpec tspec1 = tabs.newTabSpec("����������� �");
		tspec1.setIndicator(txtTabInfo1);
		tspec1.setContent(R.id.first_direction_station_list);
		tabs.addTab(tspec1);

		TextView txtTabInfo2 = createTabInfo();
		txtTabInfo2.setText(R.string.direction_b);
		TabSpec tspec2 = tabs.newTabSpec("����������� �");
		tspec2.setIndicator(txtTabInfo2);
		tspec2.setContent(R.id.second_direction_station_list);

		tabs.addTab(tspec2);
	}

	@SuppressWarnings("deprecation")
	private TextView createTabInfo() {
		Resources res = this.getResources();

		TextView txtTabInfo = new TextView(this);
		txtTabInfo.setPadding(10, 10, 10, 10);
		txtTabInfo.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);

		try {
			txtTabInfo.setTextColor(ColorStateList.createFromXml(res,
					res.getXml(R.drawable.tab_text_color)));
		} catch (Exception e) {
		}

		txtTabInfo.setGravity(Gravity.CENTER_HORIZONTAL);
		txtTabInfo.setBackgroundDrawable(res.getDrawable(R.drawable.tab_item));

		return txtTabInfo;
	}

	public void toggleFavorites() {
		if (!favoritesService.isRouteFavorite(route)) {
			favoritesService.addRouteToFavorite(route);
			invalidateOptionsMenu();
		} else {
			favoritesService.removeRouteFromFavorites(route);
			invalidateOptionsMenu();
		}
	}

	private void gotoPredictView(Station station, Station nextStation) {
		Intent gotoPredictViewActivity = new Intent(
				this.getApplicationContext(), PredictActivity.class);

		gotoPredictViewActivity.putExtra(BundleTags.DIRECTION,
				Util.getDirection(station, nextStation));
		this.startActivity(gotoPredictViewActivity);
	}

	private void gotoMapView() {
		Intent gotoMainMapViewActivity = new Intent(
				this.getApplicationContext(), MainMapView.class);

		gotoMainMapViewActivity.putExtra(BundleTags.ROUTE, route);
		this.startActivity(gotoMainMapViewActivity);
	}
}
