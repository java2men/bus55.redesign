package ru.oklogic.bus55.api.action;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.oklogic.bus55.model.GeoRoute;
import ru.oklogic.bus55.model.GeoStation;
import ru.oklogic.bus55.model.LatLong;
import ru.oklogic.bus55.model.Route;

public class GetGeoRouteAction extends BaseAction<GeoRoute> {
	@Override
	protected long getExpireTime() {
		return 15*60*1000;
	}
	
	@Override
	protected String getUrl(Parameters params) {
		Route route = (Route) params.get(ParamNames.ROUTE);

		return "http://bus.admomsk.ru/index.php/getroute/routecol_geo/"
				+ route.getId() + "/undefined/undefined";
	}

	@Override
	protected GeoRoute parse(String response, Parameters params) {
		Route route = (Route) params.get(ParamNames.ROUTE);
		GeoRoute geoRoute = new GeoRoute();
		try {
			geoRoute.setId(route.getId());
			geoRoute.setName(route.getName());
			geoRoute.setDescription(route.getDescription());
			geoRoute.setType(geoRoute.getType());

			JSONObject json = new JSONObject(response);

			JSONArray features = json.getJSONArray("features");
			JSONObject feature = features.getJSONObject(0);
			JSONObject geometry = feature.getJSONObject("geometry");
			JSONArray geometries = geometry.getJSONArray("geometries");
			JSONObject lineString = geometries.getJSONObject(0);
			JSONArray coordinates = lineString.getJSONArray("coordinates");

			for (int i = 0; i < coordinates.length(); ++i) {
				JSONArray coordinate = coordinates.getJSONArray(i);

				Double latitude = coordinate.getDouble(1);
				Double longitude = coordinate.getDouble(0);

				LatLong latLong = new LatLong(latitude, longitude);

				geoRoute.getPath().add(latLong);
			}

			JSONArray stations = json.getJSONArray("stations");

			for (int i = 0; i < stations.length(); ++i) {
				JSONObject station = stations.getJSONObject(i);
				Integer id = station.getInt("id");
				JSONArray coordinate = station.getJSONArray("coordinates");

				Double latitude = coordinate.getDouble(1);
				Double longitude = coordinate.getDouble(0);
				String name = station.getString("name");
				LatLong latLong = new LatLong(latitude, longitude);

				GeoStation llStation = new GeoStation(id.toString(), name,
						latLong);

				geoRoute.getStations().add(llStation);
			}
		} catch (JSONException e) {
		}
		return geoRoute;
	}

}
