package ru.oklogic.bus55.activity.map.event;

import ru.oklogic.bus55.model.GeoVehicle;

public interface OnGeoVehicleClick {
	void onClick(GeoVehicle vehicle);
}
