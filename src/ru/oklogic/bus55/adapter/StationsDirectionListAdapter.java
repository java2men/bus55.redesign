package ru.oklogic.bus55.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.model.Direction;

public class StationsDirectionListAdapter extends ArrayAdapter<Direction> {
	final List<Direction> items;
	final Activity context;
	
	Filter filter;
	
    public Direction getByPosition(int position)
    {
        return items.get(position);
    }	

	public StationsDirectionListAdapter(Activity context, List<Direction> items) {
		super(context, R.layout.listitem_stationdirection, items);
		this.context = context;
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflator = context.getLayoutInflater();
		View view = inflator.inflate(R.layout.listitem_stationdirection, parent, false);
		Direction direction = getByPosition(position);
		
		TextView directionNameView = (TextView) view.findViewById(R.id.station_direction_list_item_name);
		directionNameView.setText(direction.getNextStation().getName());
				
		TextView directionRoutesView = (TextView) view.findViewById(R.id.station_direction_list_item_routes);
		
		directionRoutesView.setText(direction.getDirectionDescription());		
		return view;
	}
}