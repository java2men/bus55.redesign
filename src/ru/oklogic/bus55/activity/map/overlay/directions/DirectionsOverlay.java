package ru.oklogic.bus55.activity.map.overlay.directions;

import java.util.Collection;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.map.event.OnGeoDirectionClick;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.GeoDirection;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.overlay.OverlayItem;

public class DirectionsOverlay extends BaseOverlay {
	Collection<GeoDirection> geoDirections;

	OnGeoDirectionClick onStationClick;
	
	public byte getPriority() {
		return 4;
	}
	
	public DirectionsOverlay(MapController mMapController) {
		super(mMapController);
	}

	public void setOnDirectionClick(OnGeoDirectionClick onStationClick) {
		this.onStationClick = onStationClick;
	}

	public void onItemClick(OverlayItem overlayItem) {
		if (onStationClick == null)
			return;
		DirectionOverlayItem directionOverlayItem = (DirectionOverlayItem) overlayItem;
		onStationClick.onClick(directionOverlayItem.getGeoDirection());
	}
	
	public void setGeoDirections(Collection<GeoDirection> geoDirections) {
		clearOverlayItems();
		this.geoDirections = geoDirections;

		for (GeoDirection direction : geoDirections) {
			// Load required resources
			Resources res = getResources();
			Bitmap busStationBitmap = BitmapFactory.decodeResource(res,
					R.drawable.ic_map_bus_station);

			DirectionOverlayItem directionOverlayItem = new DirectionOverlayItem(
					direction, busStationBitmap);

			addOverlayItem(directionOverlayItem);
		}
	}
}
