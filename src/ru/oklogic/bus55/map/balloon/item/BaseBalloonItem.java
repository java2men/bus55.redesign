package ru.oklogic.bus55.map.balloon.item;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.util.ScreenUtils;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class BaseBalloonItem extends LinearLayout {
	GeoPoint mGeoPoint;
	protected ImageView closeView;

	public BaseBalloonItem(Context context, GeoPoint mGeoPoint) {
		super(context);
		this.mGeoPoint = mGeoPoint;

		setOrientation(HORIZONTAL);
	}

	public void addCloseButton() {
		this.setBackgroundResource(R.drawable.map_balloon);

		closeView = new ImageView(getContext());
		closeView
				.setBackgroundResource(R.drawable.bg_map_balloon_clickable);
		closeView.setImageResource(R.drawable.btn_map_balloon_close);
		closeView.setClickable(true);
		closeView.setId(10);
		
		closeView.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				setVisibility(INVISIBLE);
			}
		});

		LayoutParams closeViewLayoutParams = new LayoutParams(
				Math.round(ScreenUtils.convertDpToPixel(25, this.getContext())),
				Math.round(ScreenUtils.convertDpToPixel(25, this.getContext()))
		);

		LayoutParams closeContainerViewParams = new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		closeContainerViewParams.gravity = Gravity.RIGHT;

		LinearLayout closeContainerView = new LinearLayout(getContext());

		this.addView(closeContainerView, closeContainerViewParams);
		closeContainerView.addView(closeView, closeViewLayoutParams);
	}

	public void setGeoPoint(GeoPoint mGeoPoint) {
		this.mGeoPoint = mGeoPoint;
	}

	public GeoPoint getGeoPoint() {
		return mGeoPoint;
	}
}
