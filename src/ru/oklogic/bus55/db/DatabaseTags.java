package ru.oklogic.bus55.db;

public class DatabaseTags {
	final public static int DATABASE_VERSION = 2;
	final public static String DATABASE_NAME = "ru.bus55.db";

	final public static String FAVORITE_DIRECTIONS_TABLE_NAME = "favorite_directions";
	final public static String FAVORITE_ROUTES_TABLE_NAME = "favorite_routes";
}
