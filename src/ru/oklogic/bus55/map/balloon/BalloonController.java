package ru.oklogic.bus55.map.balloon;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import ru.oklogic.bus55.map.balloon.item.BaseBalloonItem;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

public class BalloonController implements OnMapListener {
	BalloonView balloonView;
	MapController mapController;
	BaseBalloonItem balloonItem;
	ScreenPoint lastScreenPoint;
	MapListenerOverlay mapListenerOverlay;
	Activity activity;

	public BalloonController(BalloonView balloonView,
			MapController mapController, Activity activity) {
		this.balloonView = balloonView;
		this.mapController = mapController;
		this.activity = activity;
		mapListenerOverlay = new MapListenerOverlay(mapController);
		mapController.getOverlayManager().addOverlay(mapListenerOverlay);

		mapListenerOverlay.setOnMapListener(this);
	}

	public void onDestroy() {
		this.balloonView = null;
		this.mapController = null;
		this.activity = null;
	}

	public void show(final BaseBalloonItem balloonItem) {
		hide();
		this.balloonItem = balloonItem;

		BalloonView.LayoutParams layout = new BalloonView.LayoutParams(
				BalloonView.LayoutParams.WRAP_CONTENT,
				BalloonView.LayoutParams.WRAP_CONTENT, -1000, -1000);

		balloonView.addView(balloonItem, layout);
		updateWithDelay();
	}

	public void hide() {
		if (balloonItem != null) {
			balloonView.removeView(balloonItem);
			balloonItem = null;
			lastScreenPoint = null;
		}
	}

	public void onMap() {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				updateLayout();
			}
		});
	}

	private void updateWithDelay() {
		Timer updateVehicleTimer = new Timer();
		updateVehicleTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				activity.runOnUiThread(new Runnable() {
					public void run() {
						updateLayout();
					}
				});
			}
		}, 0);
	}

	private void updateLayout() {
		if (balloonItem == null)
			return;

		ScreenPoint screenPoint = mapController.getScreenPoint(balloonItem
				.getGeoPoint());

		if (lastScreenPoint != null
				&& screenPoint.getX() == lastScreenPoint.getX()
				&& screenPoint.getY() == lastScreenPoint.getY())
			return;

		int width = (int) balloonItem.getMeasuredWidth();
		int height = (int) balloonItem.getMeasuredHeight();

		if (width == 0 || height == 0)
			return;

		int newLeft = (int) screenPoint.getX() - width / 2;
		int newTop = (int) screenPoint.getY() - height;

		BalloonView.LayoutParams layout = new BalloonView.LayoutParams(
				BalloonView.LayoutParams.WRAP_CONTENT,
				BalloonView.LayoutParams.WRAP_CONTENT, newLeft, newTop);

		balloonItem.setLayoutParams(layout);
		lastScreenPoint = screenPoint;
	}
}
