package ru.oklogic.bus55.activity.map.overlay.directions;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.map.event.OnGeoDirectionClick;
import ru.oklogic.bus55.map.balloon.item.BaseBalloonItem;
import ru.oklogic.bus55.model.GeoDirection;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DirectionBalloonItem extends BaseBalloonItem {
	GeoDirection mGeoDirection;

	OnGeoDirectionClick onRouteStartClick;
	OnGeoDirectionClick onRouteEndClick;
	OnGeoDirectionClick onPredictDirectionClick;

	public DirectionBalloonItem(GeoDirection geoDirection, Context context) {
		super(context, geoDirection.getStation().getLatLong().toGeoPoint());

		this.mGeoDirection = geoDirection;

		TextView nameView = new TextView(context);
		nameView.setTextAppearance(context,
				android.R.style.TextAppearance_Medium);
		nameView.setText(geoDirection.getStation().getName());
		nameView.setBackgroundResource(R.drawable.bg_map_balloon_clickable);

		TextView findRouteStart = new TextView(context);
		findRouteStart.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		findRouteStart.setText(context.getString(R.string.txt_findroute_start));
		findRouteStart.setBackgroundResource(R.drawable.bg_map_balloon_clickable);

		TextView findRouteEnd = new TextView(context);
		findRouteEnd.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		findRouteEnd.setText(context.getString(R.string.txt_findroute_end));
		findRouteEnd.setBackgroundResource(R.drawable.bg_map_balloon_clickable);
		
		try {
		    XmlResourceParser parser = getResources().getXml(R.drawable.text_color);
		    ColorStateList colors = ColorStateList.createFromXml(getResources(), parser);
		    findRouteEnd.setTextColor(colors);
		    findRouteStart.setTextColor(colors);
		    nameView.setTextColor(colors);
		} catch (Exception e) {}		

		nameView.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onPredictDirectionClick.onClick(mGeoDirection);
			}
		});		
		findRouteStart.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onPredictDirectionClick.onClick(mGeoDirection);
			}
		});		
		findRouteEnd.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onPredictDirectionClick.onClick(mGeoDirection);
			}
		});
		
		LinearLayout internalLayout = new LinearLayout(context);
		internalLayout.setOrientation(LinearLayout.VERTICAL);
		internalLayout.addView(nameView);
//		internalLayout.addView(findRouteStart);
//		internalLayout.addView(findRouteEnd);
		
		this.addView(internalLayout);
		addCloseButton();
	}

	public void setOnPredictDirectionClick(OnGeoDirectionClick callback) {
		this.onPredictDirectionClick = callback;
	}
	
	public void setOnRouteStartClick(OnGeoDirectionClick callback) {
		this.onRouteStartClick = callback;
	}
	
	public void setOnRouteEndClick(OnGeoDirectionClick callback) {
		this.onRouteEndClick = callback;
	}
}
