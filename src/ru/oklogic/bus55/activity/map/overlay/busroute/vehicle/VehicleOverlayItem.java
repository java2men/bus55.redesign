package ru.oklogic.bus55.activity.map.overlay.busroute.vehicle;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import ru.oklogic.bus55.R;
import ru.oklogic.bus55.map.overlay.item.BaseOverlayItem;
import ru.oklogic.bus55.model.GeoVehicle;
import ru.oklogic.bus55.model.Route.Type;

public class VehicleOverlayItem extends BaseOverlayItem {
	GeoVehicle geoVehicle;
	Paint fillColorPaint;

	public byte getPriority() {
		return 5;
	}
	
	protected boolean isClickable() {
		return true;
	}

	public VehicleOverlayItem(GeoVehicle geoVehicle, Bitmap bitmap) {
		super(geoVehicle.getLatLong().toGeoPoint(), bitmap);

		this.geoVehicle = geoVehicle;
		
		fillColorPaint = new Paint();
		fillColorPaint.setStyle(Paint.Style.FILL);
		fillColorPaint.setColor(getColor(geoVehicle));
	}

	public GeoVehicle getGeoVehicle() {
		return geoVehicle;
	}

	public void setGeoVehicle(GeoVehicle geoVehicle) {
		this.geoVehicle = geoVehicle;
		this.setGeoPoint(geoVehicle.getLatLong().toGeoPoint());
	}

	public void draw(Canvas canvas) {

		int i = getOffsetCenterX() + getOffsetX();
		int j = getOffsetCenterY() + getOffsetY();

		Path trianglePath = new Path();
		
		float rectangleSize = getBitmap().getHeight() / 2;

		trianglePath.moveTo(rectangleSize, 0.6f * rectangleSize);
		trianglePath.lineTo(1.6f * rectangleSize, 0);
		trianglePath.lineTo(rectangleSize, -0.6f * rectangleSize);
		
		Matrix matrix = new Matrix();                                                                                           
		matrix.setTranslate(getScreenPoint().getX(), getScreenPoint().getY());
		matrix.preRotate(geoVehicle.getCourse() - 90f);
		
		trianglePath.transform(matrix);
		
		canvas.drawPath(trianglePath, fillColorPaint);

		canvas.drawBitmap(getBitmap(), (int) (getScreenPoint().getX() + i),
				(int) (getScreenPoint().getY() + j), new Paint());
	}

	public static Bitmap getBitmap(GeoVehicle geoVehicle, Resources resources) {
		int resourceId = R.drawable.ic_map_bus;
		if (geoVehicle.getRoute().getType() == Type.TRAM) {
			resourceId = R.drawable.ic_map_tram;
		}
		if (geoVehicle.getRoute().getType() == Type.TROLL) {
			resourceId = R.drawable.ic_map_troll;
		}
		Bitmap bitmap = BitmapFactory.decodeResource(resources, resourceId);

		return bitmap;
	}

	public static int getColor(GeoVehicle geoVehicle) {
		int color = Color.rgb(204, 0, 0);
		if (geoVehicle.getRoute().getType() == Type.TRAM) {
			color = Color.rgb(0,104,0);
		}
		if (geoVehicle.getRoute().getType() == Type.TROLL) {
			color = Color.rgb(0,0,204);
		}
		return color;
	}
}
