package ru.oklogic.bus55.db.fields;

public class FavoriteRoutesFields {	
	final public static String _ID = "_id";

	final public static String ROUTE_ID = "route_id";
	final public static String ROUTE_NAME = "route_name";
	final public static String ROUTE_TYPE = "route_type";
	final public static String ROUTE_DESCRIPTION = "route_description";
}
