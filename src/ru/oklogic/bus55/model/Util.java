package ru.oklogic.bus55.model;

public class Util {
	public static Direction getDirection(Station station, Station nextStation)
	{
		return new Direction(
				station.getId(),
				station,
				nextStation, 
				"8,9,99"
		);
	}
	
	public static Route.Type getRouteType(Integer n)
	{
		switch(n)
		{
		case 0: return Route.Type.BUS;
		case 1: return Route.Type.TROLL;
		case 2: return Route.Type.TRAM;
		default: return null;
		}
	}
	
	public static Route.Type getRouteType(String name)
	{
		if (name.equals("�������"))
			return Route.Type.BUS;
		
		if (name.equals("�������"))
			return Route.Type.TRAM;
		
		if (name.equals("����������"))
			return Route.Type.TROLL;
		
		return null;
	}
}
