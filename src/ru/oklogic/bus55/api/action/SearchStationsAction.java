package ru.oklogic.bus55.api.action;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.model.Station;
import ru.oklogic.bus55.util.UtilStringBuilder;
import android.content.Context;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class SearchStationsAction extends BaseAction<Collection<Station>> {
	@Override
	protected long getExpireTime() {
		return 15 * 60 * 1000;
	}

	@Override
	protected String getUrl(Parameters params) {
		return "http://bus.admomsk.ru/index.php/search";
	}

	@Override
	public AQuery call(final Parameters params,
			final Callback<Collection<Station>> callback, Context context) {
		AQuery aQuery = new AQuery(context);

		Map<String, Object> postParams = new HashMap<String, Object>();
		postParams.put("text", params.get(ParamNames.SEARCH));

		return aQuery.ajax(getUrl(params), postParams, String.class,
				new AjaxCallback<String>() {

					@Override
					public void callback(String url, String response,
							AjaxStatus status) {
						if (response != null) {
							callback.run(parse(response, params));
						}
					}
				});
	}

	@Override
	protected Collection<Station> parse(String response, Parameters params) {
		Collection<Station> stations = new LinkedList<Station>();
		try {
			Document dom = parseXML("<root>"+response+"</root>");
			Element root = dom.getDocumentElement();
			NodeList items = root.getElementsByTagName("div");
			
			for (int i = 0; i < items.getLength(); i++) {
				Element itemElement = (Element) items.item(i);
				if (!itemElement.getAttribute("class").equals("stop"))
					continue;
				Station station = new Station();
				Element aItemElement = (Element)itemElement.getElementsByTagName("a").item(0);
				station.setId(aItemElement.getAttribute("id"));
				station.setName(UtilStringBuilder.GetContentText(aItemElement));
				stations.add(station);
			}
		} catch (Exception e) {
		}

		return stations;
	}

}
