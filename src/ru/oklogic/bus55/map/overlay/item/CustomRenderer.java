package ru.oklogic.bus55.map.overlay.item;

import android.graphics.Canvas;
import ru.yandex.yandexmapkit.overlay.IRender;
import ru.yandex.yandexmapkit.overlay.OverlayItem;

public class CustomRenderer implements IRender {	
	public void draw(Canvas canvas, OverlayItem overlayItem) {
		if (!(overlayItem instanceof BaseOverlayItem))
			return;
		
		BaseOverlayItem rItem = (BaseOverlayItem) overlayItem;
		rItem.draw(canvas);
	}
}
