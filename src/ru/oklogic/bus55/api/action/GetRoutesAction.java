package ru.oklogic.bus55.api.action;

import java.util.Collection;
import java.util.LinkedList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ru.oklogic.bus55.model.Route;
import ru.oklogic.bus55.model.RouteGroup;
import ru.oklogic.bus55.util.UtilStringBuilder;

public class GetRoutesAction extends BaseAction<Collection<Route>> {
	@Override
	protected long getExpireTime() {
		return 15 * 60 * 1000;
	}

	@Override
	protected String getUrl(Parameters params) {
		RouteGroup routeGroup = (RouteGroup) params.get(ParamNames.ROUTE_GROUP);

		return "http://t.bus55.ru/index.php/app/get_routes/"
				+ routeGroup.getName();
	}

	@Override
	protected Collection<Route> parse(String response, Parameters params) {
		Collection<Route> routes = new LinkedList<Route>();
		try {
			Document dom = parseXML(response);
			Element root = dom.getDocumentElement();
			NodeList items = root.getElementsByTagName("a");
			for (int i = 0; i < items.getLength(); i++) {
				Route route = new Route();
				Element itemElement = (Element) items.item(i);
				Element typeElement = (Element) itemElement
						.getElementsByTagName("span").item(0);
				String sRouteId = itemElement.getAttribute("id");
				String sRouteNumberEx = sRouteId.split("_")[1];
				String sRouteNumber = UtilStringBuilder
						.GetContentText(itemElement);
				String sType = UtilStringBuilder.GetContentText(typeElement);

				route.setId(sRouteNumberEx);
				route.setName(sRouteNumber);

				if (sType.contains("�����")) {
					route.setType(ru.oklogic.bus55.model.Route.Type.TROLL);
				} else if (sType.contains("����")) {
					route.setType(ru.oklogic.bus55.model.Route.Type.TRAM);
				} else {
					route.setType(ru.oklogic.bus55.model.Route.Type.BUS);
				}

				Element descriptionElement = (Element) itemElement
						.getElementsByTagName("div").item(0);
				String routeDescription = descriptionElement.getChildNodes()
						.item(0).getNodeValue();
				route.setDescription(routeDescription);
				routes.add(route);
			}
		} catch (Exception e) {
		}

		return routes;
	}

}
