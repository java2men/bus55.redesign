package ru.oklogic.bus55.model;

import java.io.Serializable;

import ru.yandex.yandexmapkit.utils.GeoPoint;

public class LatLong implements Serializable {
	private static final long serialVersionUID = -6822885828942141085L;

	double latitude;
	double longitude;

	public LatLong() {
	}

	public LatLong(double d, double e) {
		this.latitude = d;
		this.longitude = e;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public GeoPoint toGeoPoint() {
		return new GeoPoint(latitude, longitude);
	}
	
	public Double getPseudoDistance(LatLong latlong) {
		Double result = 0.0;
		result += Math.abs(latitude - latlong.latitude);
		result += Math.abs(longitude - latlong.longitude);
		return result;
	}
}
