package ru.oklogic.bus55.model;

import java.io.Serializable;

import ru.oklogic.bus55.R;


public class Route implements Serializable{
	private static final long serialVersionUID = 3766746152528947987L;
	
	Type type;
	String name;
	String id;
	String routeDescription;
	
	public Route(){	}
	
	public Route(
			String name, 
			Type type,
			String id,
			String routeDescription)
	{
		this.name = name;
		this.type = type;
		this.id = id;
		this.routeDescription = routeDescription;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public Type getType(){
		return this.type;
	}
	
	public void setType(Type type){
		this.type = type;
	}
	
	public void setType(String type){
		this.type = Type.parse(type);
	}

	public String getId(){
		return this.id;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public String getDescription(){
		return this.routeDescription;
	}
	
	public void setDescription(String routeDescription){
		this.routeDescription = routeDescription;
	}
	
	public enum Type
	{
		BUS("bus"),
		TRAM("tram"),
		TROLL("troll");

		private String text;

		Type(String text) {
			this.text = text;
		}

		public String toString() {
			return this.text;
		}

		public static Type parse(String text) {
			if (text != null) {
		    	for (Type b : Type.values()) {
		    	  	if (text.equalsIgnoreCase(b.text)) {
		        		return b;
		        	}
		      	}
		    }
		    return null;
		}
		
		public int getResourceIndex()
		{
			if (this == Type.BUS)
				return R.string.bus;
			if (this == Type.TRAM)
				return R.string.tram;
			if (this == Type.TROLL)
				return R.string.troll;
			return 0;
		}
	}
}
