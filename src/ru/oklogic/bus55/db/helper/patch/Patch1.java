package ru.oklogic.bus55.db.helper.patch;

import ru.oklogic.bus55.db.DatabaseTags;
import ru.oklogic.bus55.db.fields.FavoriteRoutesFields;
import ru.oklogic.bus55.db.helper.Patch;

public class Patch1 extends Patch{
	public void patch(android.database.sqlite.SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + DatabaseTags.FAVORITE_ROUTES_TABLE_NAME + " ("
                + FavoriteRoutesFields._ID + " INTEGER PRIMARY KEY autoincrement,"
                + FavoriteRoutesFields.ROUTE_ID + " TEXT,"
                + FavoriteRoutesFields.ROUTE_NAME + " TEXT,"
                + FavoriteRoutesFields.ROUTE_TYPE + " TEXT,"
                + FavoriteRoutesFields.ROUTE_DESCRIPTION + " TEXT"
                + ");");
	};
}
