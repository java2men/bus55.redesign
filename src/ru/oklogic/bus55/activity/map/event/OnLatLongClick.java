package ru.oklogic.bus55.activity.map.event;

import ru.oklogic.bus55.model.LatLong;

public interface OnLatLongClick {
	void onClick(LatLong latLong);
}
