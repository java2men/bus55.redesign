package ru.oklogic.bus55.db.service;

import java.util.Collection;
import java.util.LinkedList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import ru.oklogic.bus55.db.DatabaseTags;
import ru.oklogic.bus55.db.fields.FavoriteRoutesFields;
import ru.oklogic.bus55.db.helper.DatabaseHelper;
import ru.oklogic.bus55.model.Route;

public class FavoriteRoutesService {
	DatabaseHelper databaseHelper;

	public FavoriteRoutesService(Context context) {
		databaseHelper = new DatabaseHelper(context);
		databaseHelper.getWritableDatabase();
	}

	public Collection<Route> getFavoriteRoutes() {
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		try {
			Cursor cursor = db.query(DatabaseTags.FAVORITE_ROUTES_TABLE_NAME, null, null, null,
					null, null, FavoriteRoutesFields.ROUTE_NAME);

			try {
				Collection<Route> routes = new LinkedList<Route>();

				while (cursor.moveToNext()) {
					String routeId = cursor.getString(cursor
							.getColumnIndex(FavoriteRoutesFields.ROUTE_ID));
					String routeName = cursor.getString(cursor
							.getColumnIndex(FavoriteRoutesFields.ROUTE_NAME));
					String routeType = cursor.getString(cursor
							.getColumnIndex(FavoriteRoutesFields.ROUTE_TYPE));
					String routeDescription = cursor.getString(cursor
							.getColumnIndex(FavoriteRoutesFields.ROUTE_DESCRIPTION));
					

					Route route = new Route();
					route.setDescription(routeDescription);
					route.setId(routeId);
					route.setName(routeName);
					route.setType(routeType);

					routes.add(route);
				}

				return routes;
			} finally {
				cursor.close();
			}
		} finally {
			db.close();
		}
	}

	public boolean isRouteFavorite(Route route) {
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		try {
			String where = FavoriteRoutesFields.ROUTE_ID + "=?";
			
			Cursor cursor = db.query(DatabaseTags.FAVORITE_ROUTES_TABLE_NAME,
					new String[] { FavoriteRoutesFields.ROUTE_ID }, where, new String[] {
					route.getId() },
					null, null, null, null);
			try {
				Boolean isFavorite = cursor.getCount() > 0;
				return isFavorite;
			} finally {
				cursor.close();
			}
		} finally {
			db.close();
		}
	}

	public void addRouteToFavorite(Route route) {
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();

			values.put(FavoriteRoutesFields.ROUTE_ID, route.getId());
			values.put(FavoriteRoutesFields.ROUTE_NAME, route.getName());
			values.put(FavoriteRoutesFields.ROUTE_TYPE, route.getType().toString());
			values.put(FavoriteRoutesFields.ROUTE_DESCRIPTION, route.getDescription());
			db.insert(DatabaseTags.FAVORITE_ROUTES_TABLE_NAME, null, values);
		} finally {
			db.close();
		}
	}

	public void removeRouteFromFavorites(Route route) {
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		try {
			String where = FavoriteRoutesFields.ROUTE_ID + "=?";
			db.delete(DatabaseTags.FAVORITE_ROUTES_TABLE_NAME, where, new String[] {
					route.getId() });
		} finally {
			db.close();
		}
	}
}
