package ru.oklogic.bus55.activity.map.overlay.global;

import java.util.LinkedList;
import java.util.List;

import ru.oklogic.bus55.activity.map.event.OnMapClickListener;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.LatLong;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

public class KeyListenerOverlay extends BaseOverlay {
	List<OnMapClickListener> onLongPressListeners;
	
	public byte getPriority() {
		return 4;
	}
	
	public KeyListenerOverlay(MapController mMapController) {
		super(mMapController);
		onLongPressListeners = new LinkedList<OnMapClickListener>();
	}
	
	@Override
	public boolean onLongPress(float x, float y)
	{
		super.onLongPress(x, y);
		ScreenPoint sp = new ScreenPoint(x, y);
		GeoPoint gp = this.getMapController().getGeoPoint(sp);
		LatLong latLong = new LatLong(gp.getLat(), gp.getLon());
		
		for (OnMapClickListener clickListerner: onLongPressListeners)
		{
			clickListerner.onMapClick(latLong);
		}
		
		return false;
	}
	
	public void setOnLongPressListener(OnMapClickListener clickListener)
	{
		onLongPressListeners.add(clickListener);
	}
	
	public void unsetOnLongPressListener(OnMapClickListener clickListener)
	{
		onLongPressListeners.remove(clickListener);
	}
}
