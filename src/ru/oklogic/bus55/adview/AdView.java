package ru.oklogic.bus55.adview;

import ru.adfox.android.AdFoxView;
import ru.oklogic.bus55.R;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class AdView extends RelativeLayout {

	public AdView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
//		initBanner(context);
	}
	
	public AdView(Context context, AttributeSet attrs) {
		super(context, attrs);
//		initBanner(context);
	}
	
	public AdView(Context context) {
		super(context);
//		initBanner(context);
	}
	
	//Override
	public void onAttachedToWindow()
	{
		initBanner(this.getContext());
	}

	private void initBanner(Context context) {
		int height = 50;
		int width = 300;
		
		ViewGroup.LayoutParams containerParams = this.getLayoutParams();
		containerParams.width = width;
		containerParams.height = height;
		this.setLayoutParams(containerParams);

		String bannerUrl = context.getResources().getString(R.string.banner_url);
		final AdFoxView adView = new AdFoxView(context, bannerUrl, width, height);
		adView.setUseFlash(false);
		adView.setSkipDialog(true);
		adView.setProgressbar(false);
		
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

		this.addView(adView, layoutParams);
	}

	@SuppressWarnings("unused")
	private float convertDpToPixel(float dp, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float px = dp * (metrics.densityDpi/160f);
	    return px;
	}
}
