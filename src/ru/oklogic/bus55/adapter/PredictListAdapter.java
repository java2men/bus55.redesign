package ru.oklogic.bus55.adapter;

import java.util.List;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.model.Predict;
import ru.oklogic.bus55.util.UtilStringBuilder;
import android.app.Activity;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class PredictListAdapter extends ArrayAdapter<Predict> {
	final List<Predict> items;
	final Activity context;
	
	Filter filter;
	
    public Predict getByPosition(int position)
    {
        return items.get(position);
    }	

	public PredictListAdapter(Activity context, List<Predict> items) {
		super(context, R.layout.listitem_predict, items);
		this.context = context;
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflator = context.getLayoutInflater();
		View view = inflator.inflate(R.layout.listitem_predict, parent, false);
		Predict predict = getByPosition(position);
		
		TextView routeName = (TextView) view.findViewById(R.id.predict_list_item_route);
		StringBuilder routeNameBuilder = new StringBuilder();
		routeNameBuilder.append(predict.getRoute().getName());
		routeNameBuilder.append(" ");
		Resources resource = getContext().getResources();
		String routeType = resource.getString(predict.getRoute().getType().getResourceIndex());
		routeNameBuilder.append(routeType);
		routeName.setText(routeNameBuilder.toString());
				
		TextView routeDescription = (TextView) view.findViewById(R.id.predict_list_item_route_description);
		
		routeDescription.setText(UtilStringBuilder.GetRouteDescription(
				predict.getRoute(), getContext().getResources()));
		
		TextView predictTime = (TextView) view.findViewById(R.id.predict_item_time);
		predictTime.setText(predict.getDate());
		
		return view;
	}
}