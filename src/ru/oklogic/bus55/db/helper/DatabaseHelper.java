package ru.oklogic.bus55.db.helper;

import ru.oklogic.bus55.db.DatabaseTags;
import ru.oklogic.bus55.db.fields.FavoriteDirectionsFields;
import ru.oklogic.bus55.db.fields.FavoriteRoutesFields;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, DatabaseTags.DATABASE_NAME, null, DatabaseTags.DATABASE_VERSION);
    }

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + DatabaseTags.FAVORITE_DIRECTIONS_TABLE_NAME + " ("
                + FavoriteDirectionsFields._ID + " INTEGER PRIMARY KEY autoincrement,"
                + FavoriteDirectionsFields.STATION_ID + " TEXT,"
                + FavoriteDirectionsFields.STATION_NAME + " TEXT,"
                + FavoriteDirectionsFields.NEXT_STATION_ID + " TEXT,"
                + FavoriteDirectionsFields.NEXT_STATION_NAME + " TEXT,"
                + FavoriteDirectionsFields.DESCRIPTION + " TEXT,"
                + FavoriteDirectionsFields.DIRECTION_ID + " TEXT"
                + ");");
		db.execSQL("CREATE TABLE " + DatabaseTags.FAVORITE_ROUTES_TABLE_NAME + " ("
                + FavoriteRoutesFields._ID + " INTEGER PRIMARY KEY autoincrement,"
                + FavoriteRoutesFields.ROUTE_ID + " TEXT,"
                + FavoriteRoutesFields.ROUTE_NAME + " TEXT,"
                + FavoriteRoutesFields.ROUTE_TYPE + " TEXT,"
                + FavoriteRoutesFields.ROUTE_DESCRIPTION + " TEXT"
                + ");");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int start, int end) {
		PatchController patchController = new PatchController();
		patchController.patch(db, start, end);
	}
}