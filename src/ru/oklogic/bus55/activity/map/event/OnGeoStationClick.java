package ru.oklogic.bus55.activity.map.event;

import ru.oklogic.bus55.model.GeoStation;

public interface OnGeoStationClick {
	void onClick(GeoStation station);
}
