package ru.oklogic.bus55.model;

public class GeoVehicle {
	Integer id;
	LatLong latLong;
	Float course;
	Route route;
	String info;

	public GeoVehicle() {

	}

	public GeoVehicle(Integer id, LatLong latLong, Float course, Route route, String info) {
		this.latLong = latLong;
		this.course = course;
		this.route = route;
		this.id = id;
		this.info = info;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof GeoVehicle))
			return false;

		GeoVehicle geoVehicle = (GeoVehicle) o;

		if (!geoVehicle.getId().equals(geoVehicle))
			return false;

		return true;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String stateNumber) {
		this.info = stateNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LatLong getLatLong() {
		return latLong;
	}

	public void setLatLong(LatLong latLong) {
		this.latLong = latLong;
	}

	public Float getCourse() {
		return course;
	}

	public void setCourse(Float course) {
		this.course = course;
	}

	public Route getRoute() {
		return route;
	}

	protected void setRoute(Route route) {
		this.route = route;
	}

}
