package ru.oklogic.bus55.activity.map.event;

import ru.oklogic.bus55.model.Station;

public interface OnStationClick {
	void onClick(Station station, Station nextStation);
}
