package ru.oklogic.bus55.activity.map.overlay.busroute.vehicle;

import android.R;
import android.content.Context;
import android.content.res.Resources;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.oklogic.bus55.map.balloon.item.BaseBalloonItem;
import ru.oklogic.bus55.model.GeoVehicle;
import ru.oklogic.bus55.model.Route;
import ru.yandex.yandexmapkit.utils.GeoPoint;

public class VehicleBalloonItem extends BaseBalloonItem {
	GeoVehicle geoVehicle;
	TextView routeNameView;
	TextView routeInfoView;
	
	LinearLayout internalLayout;

	public VehicleBalloonItem(GeoVehicle geoVehicle, Context context) {
		super(context, geoVehicle.getLatLong().toGeoPoint());

		this.geoVehicle = geoVehicle;

		Route route = geoVehicle.getRoute();
		Route.Type type = route.getType();
		Resources resources = context.getResources();

		routeNameView = new TextView(context);
		routeNameView.setTextAppearance(context, R.style.TextAppearance_Medium);
		routeNameView.setText(String.format("%s %s",
				resources.getString(type.getResourceIndex()), route.getName()));

		routeInfoView = new TextView(context);
		routeInfoView.setTextAppearance(context, R.style.TextAppearance_Small);
		routeInfoView.setText(geoVehicle.getInfo());

		internalLayout = new LinearLayout(context);
		internalLayout.setOrientation(LinearLayout.VERTICAL);
		internalLayout.addView(routeNameView);
		internalLayout.addView(routeInfoView);
		
		this.addView(internalLayout);
		
		addCloseButton();
	}

	public GeoPoint getGeoPoint() {
		return geoVehicle.getLatLong().toGeoPoint();
	}

}
