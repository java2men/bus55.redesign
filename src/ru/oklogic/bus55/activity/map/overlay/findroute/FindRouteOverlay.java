package ru.oklogic.bus55.activity.map.overlay.findroute;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.map.event.OnGeoStationClick;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.GeoStation;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.overlay.OverlayItem;

public class FindRouteOverlay extends BaseOverlay {

	GeoStation startGeoStation = null;
	GeoStation endGeoStation = null;

	OnGeoStationClick onStationClick;
	RouteClosingOverlayItem startStationOverlayItem;
	RouteClosingOverlayItem endStationOverlayItem;

	public byte getPriority() {
		return 4;
	}

	public FindRouteOverlay(MapController mMapController) {
		super(mMapController);
	}

	public void setOnDirectionClick(OnGeoStationClick onStationClick) {
		this.onStationClick = onStationClick;
	}

	public void onItemClick(OverlayItem overlayItem) {
		if (onStationClick == null)
			return;
		if (RouteClosingOverlayItem.class.isInstance(overlayItem)) {
			RouteClosingOverlayItem directionOverlayItem = (RouteClosingOverlayItem) overlayItem;
			onStationClick.onClick(directionOverlayItem.getGeoStation());
			return;
		}
	}

	public void clear() {
		startGeoStation = null;
		endGeoStation = null;
	}

	public void setStartStation(GeoStation station) {
		startGeoStation = station;
		reinitOverlayItems();
	}

	public void setEndStation(GeoStation station) {
		endGeoStation = station;
		reinitOverlayItems();
	}

	private void reinitOverlayItems() {
		clearOverlayItems();
		Resources res = getResources();
		if (startGeoStation != null) {
			Bitmap bitmap = BitmapFactory.decodeResource(res,
					R.drawable.ic_map_bus_station);
			startStationOverlayItem = new RouteClosingOverlayItem(startGeoStation, bitmap);
			addOverlayItem(startStationOverlayItem);
		}
		if (endGeoStation != null) {
			Bitmap bitmap = BitmapFactory.decodeResource(res,
					R.drawable.ic_map_bus_station);
			endStationOverlayItem = new RouteClosingOverlayItem(endGeoStation, bitmap);
			addOverlayItem(endStationOverlayItem);
		}
	}
}
