package ru.oklogic.bus55.activity.map.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import android.os.Bundle;

import ru.oklogic.bus55.R;



import ru.oklogic.bus55.activity.map.BaseLayerController;
import ru.oklogic.bus55.activity.map.MainMapView;
import ru.oklogic.bus55.activity.map.event.OnGeoStationClick;
import ru.oklogic.bus55.activity.map.event.OnLatLongClick;
import ru.oklogic.bus55.activity.map.event.OnStationClick;
import ru.oklogic.bus55.map.balloon.BalloonController;
import ru.oklogic.bus55.map.balloon.BalloonView;
import ru.oklogic.bus55.model.GeoStation;
import ru.oklogic.bus55.model.LatLong;
import ru.oklogic.bus55.model.Station;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;

public class SuperController {

	MainMapView mainMapView;
	List<BaseLayerController> controllers;

	BalloonController balloonController;
	GlobalLayerController globalLayerController;
	DirectionLayerController directionLayerController;
	BusRouteLayerController busRouteLayerController;
	FindRouteController findRouteLayerController;

	MapController mMapController;
	LocationController locationController;

	OnGeoStationClick onRouteStartClick;
	OnGeoStationClick onRouteEndClick;

	public SuperController(MainMapView mainMapView) {
		this.mainMapView = mainMapView;
		initControllers();
	}

	public List<BaseLayerController> getControllers() {
		return controllers;
	}

	public void destroy() {
		balloonController.onDestroy();
	}

	public void pause() {
		for (BaseLayerController controller : controllers) {
			controller.pause();
		}
	}

	public void resume() {
		for (BaseLayerController controller : controllers) {
			controller.resume();
		}
	}

	public void readExtras(Bundle bundle) {
		for (BaseLayerController controller : controllers) {
			controller.readExtras(bundle);
		}
	}

	private void initControllers() {
		final MapView mapView = (MapView) mainMapView.findViewById(R.id.map);
		mMapController = mapView.getMapController();

		BalloonView balloonContainer = (BalloonView) mainMapView
				.findViewById(R.id.balloon_container);
		balloonController = new BalloonController(balloonContainer,
				mMapController, mainMapView);

		controllers = new LinkedList<BaseLayerController>();

		initBusRouteLayerController();
		initDirectionLayerController();
		initGlobalLayerController();
		initFindRouteLayerController();
		initLocationController();

		controllers.add(globalLayerController);
		controllers.add(directionLayerController);
		controllers.add(busRouteLayerController);
		controllers.add(locationController);
	}

	private void initBusRouteLayerController() {
		busRouteLayerController = new BusRouteLayerController(mMapController,
				balloonController);
		busRouteLayerController.setOnShowPredictClick(new OnStationClick() {
			public void onClick(Station station, Station nextStation) {
				mainMapView.gotoPredictView(station, nextStation);
			}
		});
	}

	private void initDirectionLayerController() {
		directionLayerController = new DirectionLayerController(mMapController,
				balloonController);
		directionLayerController.setOnShowPredictClick(new OnStationClick() {
			public void onClick(Station station, Station nextStation) {
				mainMapView.gotoPredictView(station, nextStation);
			}
		});
	}

	private void initGlobalLayerController() {
		globalLayerController = new GlobalLayerController(mMapController,
				balloonController);
		globalLayerController.setOnShowStationsHereClick(new OnLatLongClick() {
			public void onClick(LatLong latLong) {
				directionLayerController.selectLocation(latLong);
			}
		});
	}

	private void initFindRouteLayerController() {
		findRouteLayerController = new FindRouteController(mMapController,
				balloonController);

		onRouteStartClick = new OnGeoStationClick() {
			@Override
			public void onClick(GeoStation station) {
				findRouteLayerController.setStartStation(station);
			}
		};
		onRouteEndClick = new OnGeoStationClick() {
			@Override
			public void onClick(GeoStation station) {
				findRouteLayerController.setEndStation(station);
			}
		};

		globalLayerController.setOnRouteStartClick(onRouteStartClick);
		directionLayerController.setOnRouteStartClick(onRouteStartClick);
		busRouteLayerController.setOnRouteStartClick(onRouteStartClick);
		globalLayerController.setOnRouteEndClick(onRouteEndClick);
		directionLayerController.setOnRouteEndClick(onRouteEndClick);
		busRouteLayerController.setOnRouteEndClick(onRouteEndClick);
	}

	private void initLocationController() {
		Collection<BaseLayerController> localControllers = new ArrayList<BaseLayerController>();
		localControllers.add(globalLayerController);
		localControllers.add(directionLayerController);
		localControllers.add(busRouteLayerController);

		locationController = new LocationController(mainMapView,
				mMapController, localControllers, balloonController);
	}
}
