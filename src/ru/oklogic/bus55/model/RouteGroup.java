package ru.oklogic.bus55.model;

import java.io.Serializable;

public class RouteGroup implements Serializable{
	private static final long serialVersionUID = -6308046843142714329L;
	
	String name;
	
	public RouteGroup(
			String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
