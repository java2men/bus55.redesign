package ru.oklogic.bus55.api;

import java.util.Collection;
import java.util.LinkedList;

import ru.oklogic.bus55.api.action.GetDirectionsAction;
import ru.oklogic.bus55.api.action.GetDirectionsAroundLocationAction;
import ru.oklogic.bus55.api.action.GetGeoDirectionsGeoLocAction;
import ru.oklogic.bus55.api.action.GetGeoRouteAction;
import ru.oklogic.bus55.api.action.GetGeoRouteVehicleListAction;
import ru.oklogic.bus55.api.action.GetPredictAction;
import ru.oklogic.bus55.api.action.GetRouteStationsAction;
import ru.oklogic.bus55.api.action.GetRoutesAction;
import ru.oklogic.bus55.api.action.GetStationsAction;
import ru.oklogic.bus55.api.action.Parameters;
import ru.oklogic.bus55.api.action.SearchStationsAction;
import ru.oklogic.bus55.model.Direction;
import ru.oklogic.bus55.model.GeoDirection;
import ru.oklogic.bus55.model.GeoRoute;
import ru.oklogic.bus55.model.GeoVehicle;
import ru.oklogic.bus55.model.Predict;
import ru.oklogic.bus55.model.Route;
import ru.oklogic.bus55.model.RouteGroup;
import ru.oklogic.bus55.model.Station;
import android.content.Context;
import android.util.Pair;

import com.androidquery.AQuery;

public class Bus55API {
	static Collection<Station> stations;

	public static Collection<RouteGroup> getRouteGroupList() {
		Collection<RouteGroup> routes = new LinkedList<RouteGroup>();
		routes.add(new RouteGroup("1"));
		routes.add(new RouteGroup("2"));
		routes.add(new RouteGroup("3"));
		routes.add(new RouteGroup("4"));
		routes.add(new RouteGroup("5"));
		routes.add(new RouteGroup("6"));
		routes.add(new RouteGroup("7"));
		routes.add(new RouteGroup("8"));
		routes.add(new RouteGroup("9"));

		return routes;
	}
	
	public static AQuery searchStationList(Parameters params,
			Callback<Collection<Station>> callback, Context context) {
		return new SearchStationsAction().call(params, callback, context);
	}

	public static AQuery getStationList(Parameters params,
			Callback<Collection<Station>> callback, Context context) {
		return new GetStationsAction().call(params, callback, context);
	}

	public static AQuery getDirectionListAroundLocation(Parameters params,
			Callback<Collection<Direction>> callback, Context context) {
		return new GetDirectionsAroundLocationAction().call(params, callback,
				context);
	}

	public static AQuery getDirectionList(Parameters params,
			Callback<Collection<Direction>> callback, Context context) {
		return new GetDirectionsAction().call(params, callback, context);
	}

	public static AQuery getPredict(Parameters params,
			Callback<Collection<Predict>> callback, Context context) {
		return new GetPredictAction().call(params, callback, context);
	}

	public static AQuery getRouteList(Parameters params,
			Callback<Collection<Route>> callback, Context context) {
		return new GetRoutesAction().call(params, callback, context);
	}

	public static AQuery getRouteStationList(Parameters params,
			Callback<Pair<Collection<Station>, Collection<Station>>> callback,
			Context context) {
		return new GetRouteStationsAction().call(params, callback, context);
	}

	public static AQuery getGeoRoute(Parameters params,
			Callback<GeoRoute> callback, Context context) {
		return new GetGeoRouteAction().call(params, callback, context);
	}

	public static AQuery getGeoRouteVehicleList(Parameters params,
			Callback<Collection<GeoVehicle>> callback, Context context) {
		return new GetGeoRouteVehicleListAction().call(params, callback,
				context);
	}

	public static AQuery getDirectionsGeoLoc(Parameters params,
			Callback<Collection<GeoDirection>> callback, Context context) {
		return new GetGeoDirectionsGeoLocAction().call(params, callback,
				context);
	}

	public static AQuery findRoute(Parameters params,
			Callback<Collection<GeoDirection>> callback, Context context) {
		return new GetGeoDirectionsGeoLocAction().call(params, callback,
				context);
	}
}
