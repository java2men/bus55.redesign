package ru.oklogic.bus55.api.action;

public enum ParamNames {
	DIRECTION,
	LOCATION,
	ROUTE,
	ROUTE_GROUP,
	STATION,
	SEARCH
}
