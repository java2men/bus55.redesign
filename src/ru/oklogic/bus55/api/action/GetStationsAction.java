package ru.oklogic.bus55.api.action;

import java.util.ArrayList;
import java.util.Collection;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;

import com.androidquery.AQuery;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.model.Station;

public class GetStationsAction extends BaseAction<Collection<Station>> {

	@Override
	protected String getUrl(Parameters params) {
		return "";
	}

	@Override
	protected Collection<Station> parse(String response, Parameters params) {
		return null;
	}

	@Override
	public AQuery call(Parameters params, Callback<Collection<Station>> callback,
			Context context) {
		ArrayList<Station> stations = new ArrayList<Station>();
		Resources res = context.getResources();
		XmlResourceParser stationXml = res.getXml(R.xml.stations);

		int eventType = -1;
		while (eventType != XmlResourceParser.END_DOCUMENT) {
			if (eventType == XmlResourceParser.START_TAG) {

				String strNode = stationXml.getName();
				if (strNode.equals("station")) {
					stations.add(new Station(stationXml.getAttributeValue(null,
							"id"), stationXml.getAttributeValue(null, "name")));
				}
			}

			try {
				eventType = stationXml.next();
			} catch (Exception e) {
			}
		}

		callback.run(stations);

		return null;
	}

}
