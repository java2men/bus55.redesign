package ru.oklogic.bus55.model;

import java.io.Serializable;

public class Station implements Serializable{
	private static final long serialVersionUID = -8619261706855406340L;

	String id;
	String name;
	
	public Station(){}
	
	public Station(
			String id, 
			String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
}
