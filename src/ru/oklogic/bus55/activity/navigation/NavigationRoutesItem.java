package ru.oklogic.bus55.activity.navigation;

import android.content.Context;
import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.routes.RouteGroupListActivity;

public class NavigationRoutesItem extends NavigationItem {
	Context context;	
	public NavigationRoutesItem(Context context) {
		this.context = context;
	}
	public int getIcon() {
		return R.drawable.btn_main_bus;
	}
	public Class<?> getActivityClass() {
		return RouteGroupListActivity.class;
	}
	@Override
	public String getTitle() {
		return context.getString(R.string.routes_button);
	}
}
