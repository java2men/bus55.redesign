package ru.oklogic.bus55.activity.favorites;

import ru.oklogic.bus55.model.Route;

public interface OnFavoriteRouteClick {
	void onClick(Route route);
}
