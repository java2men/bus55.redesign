package ru.oklogic.bus55.activity.map.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import com.androidquery.AQuery;

import android.os.Bundle;




import ru.oklogic.bus55.BundleTags;
import ru.oklogic.bus55.activity.map.BaseLayerController;
import ru.oklogic.bus55.activity.map.event.OnGeoStationClick;
import ru.oklogic.bus55.activity.map.event.OnGeoVehicleClick;
import ru.oklogic.bus55.activity.map.event.OnStationClick;
import ru.oklogic.bus55.activity.map.overlay.busroute.BusRouteOverlay;
import ru.oklogic.bus55.activity.map.overlay.busroute.BusRouteStationsOverlay;
import ru.oklogic.bus55.activity.map.overlay.busroute.BusRouteVehicleOverlay;
import ru.oklogic.bus55.activity.map.overlay.busroute.station.StationBalloonItem;
import ru.oklogic.bus55.activity.map.overlay.busroute.vehicle.VehicleBalloonItem;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.api.action.ParamNames;
import ru.oklogic.bus55.api.action.Parameters;
import ru.oklogic.bus55.map.balloon.BalloonController;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.GeoRoute;
import ru.oklogic.bus55.model.GeoStation;
import ru.oklogic.bus55.model.GeoVehicle;
import ru.oklogic.bus55.model.Route;

import ru.yandex.yandexmapkit.MapController;

public class BusRouteLayerController extends BaseLayerController {
	ArrayList<BaseOverlay> overlays;

	BusRouteOverlay busRouteOverlay;
	BusRouteStationsOverlay busRouteStationsOverlay;
	BusRouteVehicleOverlay busRouteVehicleOverlay;

	AQuery busTripRequest;
	AQuery busVehicleRequest;
	Route route;

	Timer updateVehicleTimer;

	OnStationClick onShowPredictClick;
	OnGeoStationClick onRouteStartClick;
	OnGeoStationClick onRouteEndClick;

	GeoRoute mGeoRoute;

	public BusRouteLayerController(final MapController mMapController,
			final BalloonController balloonController) {
		super(mMapController, balloonController);

		busRouteStationsOverlay = new BusRouteStationsOverlay(mMapController);
		busRouteStationsOverlay.setOnStationClick(new OnGeoStationClick() {
			public void onClick(GeoStation station) {
				showStationBalloon(station);
			}
		});

		busRouteOverlay = new BusRouteOverlay(mMapController);
		busRouteVehicleOverlay = new BusRouteVehicleOverlay(mMapController);
		busRouteVehicleOverlay.setOnVehicleClick(new OnGeoVehicleClick() {
			public void onClick(GeoVehicle vehicle) {
				VehicleBalloonItem balloon = new VehicleBalloonItem(vehicle,
						getMapController().getContext());
				getBalloonController().show(balloon);
			}
		});

		overlays = new ArrayList<BaseOverlay>();
		overlays.add(busRouteVehicleOverlay);
		overlays.add(busRouteStationsOverlay);
		overlays.add(busRouteOverlay);
	}

	private void showStationBalloon(GeoStation station) {
		StationBalloonItem balloon = new StationBalloonItem(station,
				getMapController().getContext());

		balloon.setOnPredictStationClick(new OnGeoStationClick() {
			public void onClick(GeoStation station) {
				onShowPredictClick.onClick(station, getNextStation(station));
			}
		});
		balloon.setOnRouteStartClick(new OnGeoStationClick() {
			@Override
			public void onClick(GeoStation station) {
				onRouteStartClick.onClick(station);
			}
		});
		balloon.setOnRouteEndClick(new OnGeoStationClick() {
			@Override
			public void onClick(GeoStation station) {
				onRouteEndClick.onClick(station);
			}
		});

		getBalloonController().show(balloon);
	}

	private GeoStation getNextStation(GeoStation station) {
		Collection<GeoStation> geoStations = mGeoRoute.getStations();
		Iterator<GeoStation> iterator = geoStations.iterator();

		GeoStation nextStation = iterator.next();

		while (iterator.hasNext()) {
			if (nextStation == station) {
				return iterator.next();
			}

			nextStation = iterator.next();
		}

		return geoStations.iterator().next();
	}

	@Override
	public void readExtras(Bundle bundle) {
		if (bundle.containsKey(BundleTags.ROUTE)) {
			if (busTripRequest != null) {
				busTripRequest.ajaxCancel();
			}

			route = (Route) bundle.getSerializable(BundleTags.ROUTE);
		} else {
			busRouteStationsOverlay.clearGeoRoute();
			if (busTripRequest != null) {
				busTripRequest.ajaxCancel();
				busVehicleRequest.ajaxCancel();
			}
		}
	}

	private void loadData() {
		if (route == null)
			return;

		Parameters params = new Parameters();
		params.put(ParamNames.ROUTE, route);

		busTripRequest = Bus55API.getGeoRoute(params, new Callback<GeoRoute>() {
			public void run(GeoRoute data) {
				mGeoRoute = data;
				busRouteStationsOverlay.setGeoRoute(data);
				busRouteOverlay.setGeoRoute(data);
			}
		}, getMapController().getContext());

		sheduleVehicleUpdate();
	}

	private void sheduleVehicleUpdate() {
		int delay = 1000; // delay for 1 second.
		int period = 20000; // repeat every 20 seconds.

		updateVehicleTimer = new Timer();

		updateVehicleTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (busTripRequest != null) {
					busTripRequest.ajaxCancel();
				}

				Parameters params = new Parameters();
				params.put(ParamNames.ROUTE, route);

				busVehicleRequest = Bus55API.getGeoRouteVehicleList(params,
						new Callback<Collection<GeoVehicle>>() {

							public void run(Collection<GeoVehicle> vehicles) {
								busRouteVehicleOverlay.updateVehicles(vehicles);
							}
						}, getMapController().getContext());
			}
		}, delay, period);
	}

	public OnStationClick getOnShowPredictClick() {
		return onShowPredictClick;
	}

	public void setOnRouteStartClick(OnGeoStationClick callback) {
		this.onRouteStartClick = callback;
	}

	public void setOnRouteEndClick(OnGeoStationClick callback) {
		this.onRouteEndClick = callback;
	}

	public void setOnShowPredictClick(OnStationClick onShowPredictClick) {
		this.onShowPredictClick = onShowPredictClick;
	}

	@Override
	public Collection<BaseOverlay> getOverlays() {
		return overlays;
	}

	@Override
	public void pause() {
		if (updateVehicleTimer == null)
			return;
		updateVehicleTimer.cancel();
		updateVehicleTimer.purge();
	}

	@Override
	public void resume() {
		loadData();
	}
}
