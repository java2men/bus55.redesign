package ru.oklogic.bus55.db.helper;

import java.util.ArrayList;

import android.database.sqlite.SQLiteDatabase;

import ru.oklogic.bus55.db.helper.patch.Patch1;

public class PatchController {
	ArrayList<Patch> patches;
	public PatchController() {
		patches = new ArrayList<Patch>();
		patches.add(new Patch1());
	}
	
	public void patch(SQLiteDatabase db, int start, int end) {
		for (Integer i = start; i < end; ++i) {
			patches.get(i - 1).patch(db);
		}
	}
}
