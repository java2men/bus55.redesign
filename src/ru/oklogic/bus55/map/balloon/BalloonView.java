package ru.oklogic.bus55.map.balloon;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.AbsoluteLayout;

@SuppressWarnings("deprecation")
public class BalloonView extends AbsoluteLayout {
	public BalloonView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public BalloonView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BalloonView(Context context) {
		super(context);
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}
	
	public void draw(Canvas canvas) {
		super.draw(canvas);
	}
	
	public static class LayoutParams extends AbsoluteLayout.LayoutParams {

		public LayoutParams(LayoutParams layoutParams) {
			super(layoutParams);
		}

		public LayoutParams(int arg0, int arg1, int x, int y) {
			super(arg0, arg1, x, y);
		}
	}
}
