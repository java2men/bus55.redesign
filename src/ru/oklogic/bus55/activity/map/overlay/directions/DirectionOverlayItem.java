package ru.oklogic.bus55.activity.map.overlay.directions;

import java.util.List;

import ru.oklogic.bus55.map.overlay.item.BaseOverlayItem;
import ru.oklogic.bus55.model.GeoDirection;
import ru.oklogic.bus55.model.LatLong;
import ru.yandex.yandexmapkit.MapController;
import android.graphics.Bitmap;

public class DirectionOverlayItem extends BaseOverlayItem {
	List<LatLong> routePath;
	MapController mapController;
	GeoDirection geoDirection;

	public byte getPriority() {
		return 3;
	}
	
	protected boolean isClickable() {
		return true;
	}

	public DirectionOverlayItem(GeoDirection geoDirection, Bitmap bitmap) {
		super(geoDirection.getStation().getLatLong().toGeoPoint(), bitmap);
		
		this.geoDirection = geoDirection;
	}
	
	public GeoDirection getGeoDirection() {
		return geoDirection;
	}
}
