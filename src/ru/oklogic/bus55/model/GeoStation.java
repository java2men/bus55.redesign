package ru.oklogic.bus55.model;

public class GeoStation extends Station {
	private static final long serialVersionUID = 6346666127528427540L;
	
	LatLong latLong;

	public GeoStation(){
		super();
	}
	
	public GeoStation(
			String id, 
			String name, LatLong latLong){
		super(id,name);
		this.latLong = latLong;
	}
	
	public LatLong getLatLong() {
		return latLong;
	}

	public void setLatLong(LatLong latLong) {
		this.latLong = latLong;
	}
}
