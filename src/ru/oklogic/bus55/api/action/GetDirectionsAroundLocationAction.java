package ru.oklogic.bus55.api.action;

import java.util.LinkedList;
import java.util.Collection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ru.oklogic.bus55.model.Direction;
import ru.oklogic.bus55.model.LatLong;
import ru.oklogic.bus55.model.Station;
import ru.oklogic.bus55.util.UtilStringBuilder;

public class GetDirectionsAroundLocationAction extends
		BaseAction<Collection<Direction>> {
	@Override
	protected String getUrl(Parameters params) {
		LatLong location = (LatLong) params.get(ParamNames.LOCATION);

		return "http://t.bus55.ru/index.php/app/get_stations_geoloc/"
				+ location.getLatitude() + "/" + location.getLongitude();
	}

	@Override
	protected Collection<Direction> parse(String response, Parameters params) {
		Collection<Direction> directions = new LinkedList<Direction>();
		try {
			Document dom = parseXML(response);
			Element root = dom.getDocumentElement();
			NodeList items = root.getElementsByTagName("li");
			for (int i = 0; i < items.getLength(); i++) {
				Direction direction = new Direction();
				Station station = new Station();
				Station nextStation = new Station();

				Element itemElement = (Element) items.item(i);
				Element stationElement = (Element) itemElement
						.getElementsByTagName("a").item(0);
				Element nextStationElement = (Element) itemElement
						.getElementsByTagName("div").item(0);

				String sStationId = stationElement.getAttribute("id");
				String[] sss = sStationId.split("_");
				String sStationNumberEx = sss[1];

				String nextStationName = UtilStringBuilder
						.GetContentText(nextStationElement);
				nextStationName = nextStationName.split("\n")[1];
				nextStationName = nextStationName.trim();
				nextStation.setName(nextStationName);

				direction.setId(sStationNumberEx);
				direction.setDescription("");
				station.setId(sStationNumberEx);
				String stationName = UtilStringBuilder
						.GetContentText(stationElement);
				stationName = stationName.trim();
				station.setName(stationName);
				direction.setStation(station);
				direction.setNextStation(nextStation);
				directions.add(direction);
			}
		} catch (Exception e) {
		}

		return directions;
	}

}
