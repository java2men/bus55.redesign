package ru.oklogic.bus55.map.balloon;

import java.util.LinkedList;
import java.util.List;

import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.overlay.OverlayItem;

public class MapListenerOverlay extends BaseOverlay {
	private OnMapListener mapListener;
	
	public MapListenerOverlay(MapController mMapController) {
		super(mMapController);
	}
	
	public void setOnMapListener(OnMapListener mapListener)
	{
		this.mapListener = mapListener;
	}
	
	@SuppressWarnings("rawtypes")
	public List prepareDraw() {
		mapListener.onMap();
		return new LinkedList<OverlayItem>();
	}
}
