package ru.oklogic.bus55.map;

import ru.yandex.yandexmapkit.MapView;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class Bus55MapView extends RelativeLayout {
	private CustomMapController mapController;
	private MapView mapView;

	public Bus55MapView(Context context, AttributeSet attr) {
		super(context, attr);
		initMapView(context, attr);
	}

	public CustomMapController getCustomMapController() {
		return mapController;
	}

	private void initMapView(Context context, AttributeSet attr) {
		mapView = new MapView(context, attr);

		RelativeLayout.LayoutParams mapViewLayoutParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);

		mapViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT,
				RelativeLayout.TRUE);
		mapViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,
				RelativeLayout.TRUE);
		mapViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP,
				RelativeLayout.TRUE);
		mapViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
				RelativeLayout.TRUE);

		this.addView(mapView, mapViewLayoutParams);
		mapController = new CustomMapController(mapView.getMapController());
	}
}
