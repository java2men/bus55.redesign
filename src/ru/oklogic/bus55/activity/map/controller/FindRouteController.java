package ru.oklogic.bus55.activity.map.controller;

import java.util.Collection;
import java.util.LinkedList;

import com.androidquery.AQuery;

import ru.oklogic.bus55.activity.map.BaseLayerController;
import ru.oklogic.bus55.activity.map.overlay.findroute.FindRouteOverlay;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.map.balloon.BalloonController;
import ru.oklogic.bus55.map.overlay.BaseOverlay;
import ru.oklogic.bus55.model.GeoStation;
import ru.oklogic.bus55.model.LatLong;
import ru.yandex.yandexmapkit.MapController;

public class FindRouteController extends BaseLayerController {
	Collection<BaseOverlay> overlays;
	LatLong myLocation = null;
	FindRouteOverlay findRouteOverlay;
	GeoStation startStation = null;
	GeoStation endStation = null;
	
	AQuery routeRequest;

	public FindRouteController(MapController mMapController,
			BalloonController balloonController) {
		super(mMapController, balloonController);

		overlays = new LinkedList<BaseOverlay>();
		overlays.add(findRouteOverlay = new FindRouteOverlay(mMapController));
	}

	public void setMyLocation(LatLong latLong) {
		myLocation = latLong;
	}

	public void setStartStation(GeoStation station) {
		startStation = station;
		findRouteOverlay.setStartStation(station);
	}

	public void setEndStation(GeoStation station) {
		endStation = station;
		findRouteOverlay.setEndStation(station);
	}
	
	private void updateRoute() {
		if (routeRequest != null) {
			routeRequest.ajaxCancel();
		}
		
//		routeRequest = Bus55API.findRoute(params, callback, context);
	}

	public void clear() {
		startStation = null;
		endStation = null;
		findRouteOverlay.clear();
	}

	@Override
	public Collection<BaseOverlay> getOverlays() {
		return overlays;
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

}
