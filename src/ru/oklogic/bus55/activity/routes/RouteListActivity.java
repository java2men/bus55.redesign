package ru.oklogic.bus55.activity.routes;

import java.util.LinkedList;
import java.util.Collection;

import com.androidquery.AQuery;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.activity.SecondarySherlockActivity;
import ru.oklogic.bus55.adapter.RouteListAdapter;
import ru.oklogic.bus55.api.Bus55API;
import ru.oklogic.bus55.api.Callback;
import ru.oklogic.bus55.api.action.ParamNames;
import ru.oklogic.bus55.api.action.Parameters;
import ru.oklogic.bus55.model.Route;
import ru.oklogic.bus55.model.RouteGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class RouteListActivity extends SecondarySherlockActivity {
	RouteListAdapter routeListAdapter;
	RouteGroup routeGroup;
	AQuery updateRoutesAjax;
	ListView routeListView;
	
	protected String getActivityTitle() {
		return getResources().getString(R.string.route_title);
	}

	@Override
	// Bus55API.getRoutes(routeGroup)
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_list_routes);

		routeGroup = (RouteGroup) getIntent().getExtras().getSerializable(
				"group");

		routeListView = (ListView) findViewById(R.id.route_list);

		routeListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				gotoRouteView(view, routeListAdapter.getByPosition(position));
			}
		});

		routeListAdapter = new RouteListAdapter(this, new LinkedList<Route>());
		routeListView.setAdapter(routeListAdapter);

		updateRoutes();
	}

	private void updateRoutes() {
		Parameters params = new Parameters();
		params.put(ParamNames.ROUTE_GROUP, routeGroup);
		showLoading();

		updateRoutesAjax = Bus55API.getRouteList(params,
				new Callback<Collection<Route>>() {
					public void run(Collection<Route> data) {
						hideLoading();
						routeListAdapter.clear();
						loadData(data);
					}
				}, this);
	}

	private void loadData(Collection<Route> data) {
		routeListAdapter = new RouteListAdapter(this, new LinkedList<Route>(data));
		routeListView.setAdapter(routeListAdapter);
	}
	
	public void onCancelLoading() {
		updateRoutesAjax.clear();
	}

	public void gotoRouteView(View view, Route route) {
		Intent gotoRouteViewActivity = new Intent(this.getApplicationContext(),
				RouteActivity.class);
		gotoRouteViewActivity.putExtra("route", route);
		this.startActivity(gotoRouteViewActivity);
	}
}
