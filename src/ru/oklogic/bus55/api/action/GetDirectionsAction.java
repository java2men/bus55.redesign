package ru.oklogic.bus55.api.action;

import java.util.LinkedList;
import java.util.Collection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ru.oklogic.bus55.model.Direction;
import ru.oklogic.bus55.model.Station;
import ru.oklogic.bus55.util.UtilStringBuilder;

public class GetDirectionsAction extends BaseAction<Collection<Direction>> {

	@Override
	protected long getExpireTime() {
		return 15 * 60 * 1000;
	}

	@Override
	protected String getUrl(Parameters params) {
		Station station = (Station) params.get(ParamNames.STATION);

		return "http://t.bus55.ru/index.php/app/get_dir/" + station.getId();
	}

	@Override
	protected Collection<Direction> parse(String response, Parameters params) {
		Station station = (Station) params.get(ParamNames.STATION);
		Collection<Direction> directions = new LinkedList<Direction>();
		try {
			Document dom = null;
			try {
				dom = parseXML(response);
			} catch (Exception e) {
				response = response.replace(" \"", " &quot;");
				response = response.replace("\"\"", "&quot;\"");
				dom = parseXML(response);
			}

			Element root = dom.getDocumentElement();
			NodeList items = root.getElementsByTagName("a");
			for (int i = 0; i < items.getLength(); i++) {
				Direction direction = new Direction();
				Element itemElement = (Element) items.item(i);

				String sDirectionIdEx = itemElement.getAttribute("href");
				String sDirectionId = sDirectionIdEx.split("/")[6];
				String sDirectionDescription = UtilStringBuilder
						.GetContentText(itemElement.getElementsByTagName("div")
								.item(0));

				Station nextStation = new Station();
				String sNextStationIdEx = itemElement.getAttribute("id");
				String sNextStationId = sNextStationIdEx.split("_")[2];
				String sNextStationName = UtilStringBuilder
						.GetContentText(itemElement);
				nextStation.setId(sNextStationId);
				nextStation.setName(sNextStationName);

				direction.setId(sDirectionId);
				direction.setStation(station);
				direction.setDescription(sDirectionDescription);
				direction.setNextStation(nextStation);

				directions.add(direction);
			}
		} catch (Exception e) {
		}

		return directions;
	}

}
