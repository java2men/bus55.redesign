package ru.oklogic.bus55.activity.favorites;

import ru.oklogic.bus55.model.Direction;

public interface OnFavoriteDirectionClick {
	void onClick(Direction direction);
}
