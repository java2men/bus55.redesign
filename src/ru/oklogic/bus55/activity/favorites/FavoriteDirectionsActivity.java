package ru.oklogic.bus55.activity.favorites;

import java.util.LinkedList;

import ru.oklogic.bus55.R;
import ru.oklogic.bus55.BundleTags;
import ru.oklogic.bus55.activity.HomeSherlockActivity;
import ru.oklogic.bus55.activity.favorites.adapter.FavoriteDirectionsListAdapter;
import ru.oklogic.bus55.activity.favorites.adapter.FavoriteRoutesListAdapter;
import ru.oklogic.bus55.activity.routes.RouteActivity;
import ru.oklogic.bus55.activity.stations.PredictActivity;
import ru.oklogic.bus55.db.service.FavoriteDirectionsService;
import ru.oklogic.bus55.db.service.FavoriteRoutesService;
import ru.oklogic.bus55.model.Direction;
import ru.oklogic.bus55.model.Route;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

public class FavoriteDirectionsActivity extends HomeSherlockActivity {
	FavoriteDirectionsListAdapter favoriteDirectionsListAdapter;
	FavoriteDirectionsService favoriteDirectionsService;
	FavoriteRoutesListAdapter favoriteRoutesListAdapter;
	FavoriteRoutesService favoriteRoutesService;
	ListView favoriteDirectionsList;
	ListView favoriteRoutesList;

	protected String getActivityTitle() {
		return getResources().getString(R.string.favorites_title);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_list_favorites);
		initFavoriteDirectionsList();
		initFavoriteRoutesList();
	}

	private void initFavoriteDirectionsList() {
		favoriteDirectionsService = new FavoriteDirectionsService(this);
		favoriteDirectionsList = (ListView) findViewById(R.id.favorite_directions);

		favoriteDirectionsListAdapter = new FavoriteDirectionsListAdapter(this,
				new LinkedList<Direction>(
						favoriteDirectionsService.getFavoriteDirections()));

		favoriteDirectionsListAdapter
				.setOnFavoriteDirectionToggledListener(new OnFavoriteDirectionToggled() {
					public void onToggle(Direction direction, Boolean isChecked) {
						toggleFavoriteDirection(direction, isChecked);
					}
				});
		favoriteDirectionsList.setAdapter(favoriteDirectionsListAdapter);
		favoriteDirectionsListAdapter
				.setOnFavoriteDirectionClickListener(new OnFavoriteDirectionClick() {

					public void onClick(Direction direction) {
						gotoPredict(direction);
					}
				});
	}

	private void initFavoriteRoutesList() {
		favoriteRoutesService = new FavoriteRoutesService(this);
		favoriteRoutesList = (ListView) findViewById(R.id.favorite_routes);
		favoriteRoutesListAdapter = new FavoriteRoutesListAdapter(
				this,
				new LinkedList<Route>(favoriteRoutesService.getFavoriteRoutes()));
		favoriteRoutesListAdapter
				.setOnFavoriteRouteToggledListener(new OnFavoriteRouteToggled() {
					public void onToggle(Route route, Boolean isChecked) {
						toggleFavoriteRoute(route, isChecked);
					}
				});
		favoriteRoutesList.setAdapter(favoriteRoutesListAdapter);
		favoriteRoutesListAdapter
				.setOnFavoriteRouteClickListener(new OnFavoriteRouteClick() {

					public void onClick(Route route) {
						gotoRoute(route);
					}
				});
	}

	private void gotoPredict(Direction direction) {
		Intent gotoPredictViewActivity = new Intent(
				this.getApplicationContext(), PredictActivity.class);

		gotoPredictViewActivity.putExtra(BundleTags.DIRECTION, direction);
		this.startActivity(gotoPredictViewActivity);
	}

	private void gotoRoute(Route route) {
		Intent gotoRouteViewActivity = new Intent(this.getApplicationContext(),
				RouteActivity.class);
		gotoRouteViewActivity.putExtra(BundleTags.ROUTE, route);
		this.startActivity(gotoRouteViewActivity);
	}

	private void toggleFavoriteDirection(Direction direction, Boolean isChecked) {

		if (isChecked
				&& favoriteDirectionsService.isDirectionFavorite(direction))
			return;

		if (isChecked) {
			favoriteDirectionsService.addDirectionToFavorite(direction);
			return;
		}

		favoriteDirectionsService.removeDirectionFromFavorites(direction);
	}

	private void toggleFavoriteRoute(Route route, Boolean isChecked) {

		if (isChecked && favoriteRoutesService.isRouteFavorite(route))
			return;

		if (isChecked) {
			favoriteRoutesService.addRouteToFavorite(route);
			return;
		}

		favoriteRoutesService.removeRouteFromFavorites(route);
	}
}
