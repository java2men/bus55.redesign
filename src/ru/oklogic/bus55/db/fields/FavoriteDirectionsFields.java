package ru.oklogic.bus55.db.fields;

public class FavoriteDirectionsFields {	
	final public static String _ID = "_id";
	
	final public static String STATION_NAME = "station_name";
	final public static String STATION_ID = "station_id";
	
	final public static String NEXT_STATION_NAME = "next_station_name";
	final public static String NEXT_STATION_ID = "next_station_id";
	
	final public static String DESCRIPTION = "description";
	
	final public static String DIRECTION_ID = "direction_id";
}
